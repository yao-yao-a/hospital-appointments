package com.atguigu.syt.order.service.impl;


import com.alibaba.fastjson.JSONObject;
import com.atguigu.common.service.exception.GuiguException;
import com.atguigu.common.service.utils.HttpRequestHelper;
import com.atguigu.common.util.result.ResultCodeEnum;
import com.atguigu.syt.enums.OrderStatusEnum;
import com.atguigu.syt.hosp.client.ScheduleFeignClient;
import com.atguigu.syt.model.order.OrderInfo;
import com.atguigu.syt.model.user.Patient;
import com.atguigu.syt.order.mapper.OrderInfoMapper;
import com.atguigu.syt.order.service.OrderInfoService;
import com.atguigu.syt.order.service.WxPayService;
import com.atguigu.syt.rabbit.RabbitService;
import com.atguigu.syt.rabbit.config.MQConst;
import com.atguigu.syt.user.client.PatientFeignClient;
import com.atguigu.syt.vo.hosp.ScheduleOrderVo;
import com.atguigu.syt.vo.order.OrderMqVo;
import com.atguigu.syt.vo.order.SignInfoVo;
import com.atguigu.syt.vo.sms.SmsVo;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author atguigu
 * @since 2023-07-24
 */
@Service
@Slf4j
public class OrderInfoServiceImpl extends ServiceImpl<OrderInfoMapper, OrderInfo> implements OrderInfoService {
    @Autowired
    private PatientFeignClient patientFeignClient;

    @Autowired
    private ScheduleFeignClient scheduleFeignClient;
    @Autowired
    private WxPayService wxPayService;
    @Autowired
    private RabbitService rabbitService;

    @Override
    public Long submitOrder(String scheduleId, Long patientId) {
        //1根据patientId跨模块user获取就诊人相关信息
        //就诊人数据：远程获取就诊人数据（openfeign）
        Patient patient = patientFeignClient.getPatient(patientId);
        if (patient == null) {
            throw new GuiguException(ResultCodeEnum.PARAM_ERROR);
        }

        //获取医院排班相关数据
        //2根据scheduleId跨模块hosp获取排班相关信息
        ScheduleOrderVo scheduleOrderVo = scheduleFeignClient.getScheduleOrderVo(scheduleId);
        if (scheduleOrderVo == null) {
            throw new GuiguException(ResultCodeEnum.PARAM_ERROR);
        }

        //SignInfoVo signInfoVo = hospitalFeignClient.getSignInfoVo(scheduleOrderVo.getHoscode());
        //3跨模块hosp获取医院设置信息（url、签名）
        SignInfoVo signInfoVo = new SignInfoVo();
        signInfoVo.setSignKey("8af52af00baf6aec434109fc17164aae");
        signInfoVo.setApiUrl("http://localhost:9998");
        if (signInfoVo == null) {
            throw new GuiguException(ResultCodeEnum.PARAM_ERROR);
        }
        //4判断是否还有号
        if (scheduleOrderVo.getAvailableNumber() <= 0) {
            throw new GuiguException(ResultCodeEnum.NUMBER_NO);
        }

        //创建订单对象
        OrderInfo orderInfo = new OrderInfo();

        //基本信息
        String outTradeNo = UUID.randomUUID().toString().replace("-", "");
        orderInfo.setOutTradeNo(outTradeNo); //订单号
        orderInfo.setOrderStatus(OrderStatusEnum.UNPAID.getStatus());//未支付

        //就诊人数据
        orderInfo.setPatientId(patientId);
        orderInfo.setPatientPhone(patient.getPhone());
        orderInfo.setPatientName(patient.getName());
        orderInfo.setUserId(patient.getUserId());

        //医院排班相关数据
        orderInfo.setScheduleId(scheduleId);
        BeanUtils.copyProperties(scheduleOrderVo, orderInfo);//拷贝相关属性

        //step4：调用医院端接口获取相关数据
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("hoscode", scheduleOrderVo.getHoscode());
        paramsMap.put("depcode", scheduleOrderVo.getDepcode());
        paramsMap.put("hosScheduleId", scheduleOrderVo.getHosScheduleId());
        paramsMap.put(
                "reserveDate",
                new DateTime(scheduleOrderVo.getReserveDate()).toString("yyyy-MM-dd")
        );
        paramsMap.put("reserveTime", scheduleOrderVo.getReserveTime());
        paramsMap.put("amount", scheduleOrderVo.getAmount());
        paramsMap.put("name", patient.getName());
        paramsMap.put("certificatesType", patient.getCertificatesType());
        paramsMap.put("certificatesNo", patient.getCertificatesNo());
        paramsMap.put("sex", patient.getSex());
        paramsMap.put("birthdate", patient.getBirthdate());
        paramsMap.put("phone", patient.getPhone());
        paramsMap.put("isMarry", patient.getIsMarry());
        paramsMap.put("timestamp", HttpRequestHelper.getTimestamp());

        paramsMap.put("sign", HttpRequestHelper.getSign(paramsMap, signInfoVo.getSignKey()));//标准签名
        JSONObject jsonResult = HttpRequestHelper.sendRequest(
                paramsMap, signInfoVo.getApiUrl() + "/order/submitOrder"
        );
        log.info("结果：" + jsonResult.toJSONString());

        if (jsonResult.getInteger("code") != 200) {//失败

            log.error("预约失败，"
                    + "code：" + jsonResult.getInteger("code")
                    + "，message：" + jsonResult.getString("message")
            );
            throw new GuiguException(ResultCodeEnum.FAIL.getCode(), jsonResult.getString("message"));
        }

        JSONObject data = jsonResult.getJSONObject("data");
        String hosOrderId = data.getString("hosOrderId");
        Integer number = data.getInteger("number");
        String fetchTime = data.getString("fetchTime");
        String fetchAddress = data.getString("fetchAddress");
        orderInfo.setHosOrderId(hosOrderId);
        orderInfo.setNumber(number);
        orderInfo.setFetchTime(fetchTime);
        orderInfo.setFetchAddress(fetchAddress);
        baseMapper.insert(orderInfo);

        //使用这两个数据更新平台端的最新的排班数量
        Integer reservedNumber = data.getInteger("reservedNumber");
        Integer availableNumber = data.getInteger("availableNumber");

        //目的1：更新mongodb数据库中的排班数量
        //TODO 中间件：MQ 异步解耦
        OrderMqVo orderMqVo = new OrderMqVo();
        orderMqVo.setAvailableNumber(availableNumber);
        orderMqVo.setReservedNumber(reservedNumber);
        orderMqVo.setScheduleId(scheduleId);
        //发消息
        rabbitService.sendMessage(MQConst.EXCHANGE_DIRECT_ORDER, MQConst.ROUTING_ORDER, orderMqVo);

        //目的2：给就诊人发短信 TODO：MQ
        //组装短信消息对象
        SmsVo smsVo = new SmsVo();
        smsVo.setPhone(orderInfo.getPatientPhone());
//亲爱的用户：您已预约%{hosname}%{hosdepname}%{date}就诊
//请您于%{time}至%{address}取号，
//您的就诊号码是%{number}，请您及时就诊
        smsVo.setTemplateCode("和客服申请模板编号");
        Map<String, Object> paramsSms = new HashMap<String, Object>() {{
            put("hosname", orderInfo.getHosname());
            put("hosdepname", orderInfo.getDepname());
            put("date", new DateTime(orderInfo.getReserveDate()).toString("yyyy-MM-dd"));
            put("time", orderInfo.getFetchTime());
            put("address", orderInfo.getFetchAddress());
            put("number", orderInfo.getNumber());
        }};
        smsVo.setParam(paramsSms);
//向MQ发消息
        rabbitService.sendMessage(MQConst.EXCHANGE_DIRECT_SMS, MQConst.ROUTING_SMS, smsVo);
        return orderInfo.getId(); //返回订单id
    }

    @Override
    public OrderInfo getOrderInfo(Long orderId) {
        OrderInfo orderInfo = baseMapper.selectById(orderId);
        return this.packOrderInfo(orderInfo);
    }

    /**
     * 封装订单数据
     *
     * @param orderInfo
     * @return
     */
    private OrderInfo packOrderInfo(OrderInfo orderInfo) {
        orderInfo.getParam().put(
                "orderStatusString",
                OrderStatusEnum.getStatusNameByStatus(orderInfo.getOrderStatus())
        );
        return orderInfo;
    }

    @Override
    public List<OrderInfo> selectList(Long userId) {

        LambdaQueryWrapper<OrderInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(OrderInfo::getUserId, userId);
        List<OrderInfo> orderInfoList = baseMapper.selectList(queryWrapper);
        orderInfoList.stream().forEach(this::packOrderInfo);
        return orderInfoList;
    }

    @Override
    public OrderInfo selectByOutTradeNo(String outTradeNo) {
        LambdaQueryWrapper<OrderInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(OrderInfo::getOutTradeNo, outTradeNo);
        return baseMapper.selectOne(queryWrapper);
    }

    @Override
    public void updateStatus(String outTradeNo, Integer status) {

        LambdaQueryWrapper<OrderInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(OrderInfo::getOutTradeNo, outTradeNo);

        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setOrderStatus(status);
        baseMapper.update(orderInfo, queryWrapper);
    }

    @Override
    public void cancelOrder(String outTradeNo) {
        //1根据outTradeNo查询订单信息
        OrderInfo orderInfo = this.selectByOutTradeNo(outTradeNo);
        //2判断是否已过退号截止时间，已过结束
        Date quitTime = orderInfo.getQuitTime();
        if (new DateTime(quitTime).isBeforeNow()) {
            throw new GuiguException(ResultCodeEnum.CANCEL_ORDER_NO);
        }
        //3调用医院接口确认取消预约
        Map<String, Object> params = new HashMap<>();
        params.put("hoscode", orderInfo.getHoscode());
        params.put("hosOrderId", orderInfo.getHosOrderId());
        params.put("hosScheduleId", orderInfo.getHosScheduleId());
        params.put("timestamp", HttpRequestHelper.getTimestamp());
        params.put("sign", HttpRequestHelper.getSign(params, "8af52af00baf6aec434109fc17164aae"));
        JSONObject jsonObject =
                HttpRequestHelper.sendRequest(params, "http://localhost:9998/order/updateCancelStatus");
        Integer code = jsonObject.getInteger("code");
        //4判断如果失败抛异常
        if (code.intValue() != 200) {
            throw new GuiguException(ResultCodeEnum.CANCEL_ORDER_FAIL);
        }
        //5判断订单是否已支付
        if (orderInfo.getOrderStatus().intValue() == OrderStatusEnum.PAID.getStatus()) {
            //6已支付调用接口微信退款
            //已支付，则退款
            log.info("退款");
            wxPayService.refund(outTradeNo);
            //wxPayService.refund(outTradeNo);
            //7调用接口成功，更新订单状态：取消预约，退款中
            this.updateStatus(outTradeNo, OrderStatusEnum.CANCLE_UNREFUND.getStatus());
        }
        //8如没有支付，更新订单状态：取消预约
        this.updateStatus(outTradeNo, OrderStatusEnum.CANCLE.getStatus());

        //9 TODO 发送mq消息、更新号源、发送短信通知
        JSONObject data = jsonObject.getJSONObject("data");
        Integer reservedNumber = data.getInteger("reservedNumber");
        Integer availableNumber = data.getInteger("availableNumber");
//目的1：更新mongodb数据库中的排班数量
//组装数据同步消息对象
        OrderMqVo orderMqVo = new OrderMqVo();
        orderMqVo.setAvailableNumber(availableNumber);
        orderMqVo.setReservedNumber(reservedNumber);
        orderMqVo.setScheduleId(orderInfo.getScheduleId());
//发消息
        rabbitService.sendMessage(MQConst.EXCHANGE_DIRECT_ORDER, MQConst.ROUTING_ORDER, orderMqVo);

//目的2：给就诊人发短信
//组装短信消息对象
        SmsVo smsVo = new SmsVo();
        smsVo.setPhone(orderInfo.getPatientPhone());
//亲爱的用户：您已取消%{hosname}%{hosdepname}%{date}就诊
        smsVo.setTemplateCode("和客服申请模板编号");
        Map<String, Object> paramsSms = new HashMap<String, Object>() {{
            put("hosname", orderInfo.getHosname());
            put("hosdepname", orderInfo.getDepname());
            put("date", new DateTime(orderInfo.getReserveDate()).toString("yyyy-MM-dd"));
        }};
        smsVo.setParam(paramsSms);
//向MQ发消息
        rabbitService.sendMessage(MQConst.EXCHANGE_DIRECT_SMS, MQConst.ROUTING_SMS, smsVo);
    }

    @Override
    public void patientAdvice() {

        //查询明天的预约信息
        LambdaQueryWrapper<OrderInfo> queryWrapper = new LambdaQueryWrapper<>();

        //明天
        String tomorrow = new DateTime().plusDays(1).toString("yyyy-MM-dd");
        queryWrapper.eq(OrderInfo::getReserveDate, tomorrow);
        //未取消
        queryWrapper.ne(OrderInfo::getOrderStatus, OrderStatusEnum.CANCLE.getStatus());
        List<OrderInfo> orderInfoList = baseMapper.selectList(queryWrapper);

        for (OrderInfo orderInfo : orderInfoList) {
            //短信对象
            SmsVo smsVo = new SmsVo();
            smsVo.setPhone(orderInfo.getPatientPhone());
            //就诊提醒：您已预约%{hosname}%{depname}的号源，就诊时间：%{date}，就诊人%{name}，请您合理安排出行时间
            smsVo.setTemplateCode("和客服申请模板编号");
            Map<String, Object> paramsSms = new HashMap<String, Object>() {{
                put("hosname", orderInfo.getHosname());
                put("hosdepname", orderInfo.getDepname());
                put("date", new DateTime(orderInfo.getReserveDate()).toString("yyyy-MM-dd"));
                put("name", orderInfo.getPatientName());
            }};
            smsVo.setParam(paramsSms);
            //发消息
            rabbitService.sendMessage(MQConst.EXCHANGE_DIRECT_SMS, MQConst.ROUTING_SMS, smsVo);
        }
    }
}
