package com.atguigu.syt.order.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.common.service.exception.GuiguException;
import com.atguigu.common.service.utils.HttpRequestHelper;
import com.atguigu.common.util.result.ResultCodeEnum;
import com.atguigu.syt.enums.OrderStatusEnum;
import com.atguigu.syt.model.order.OrderInfo;
import com.atguigu.syt.order.config.WxPayConfig;
import com.atguigu.syt.order.service.OrderInfoService;
import com.atguigu.syt.order.service.PaymentInfoService;
import com.atguigu.syt.order.service.RefundInfoService;
import com.atguigu.syt.order.service.WxPayService;
import com.wechat.pay.java.core.RSAAutoCertificateConfig;
import com.wechat.pay.java.core.exception.HttpException;
import com.wechat.pay.java.core.exception.MalformedMessageException;
import com.wechat.pay.java.core.exception.ServiceException;
import com.wechat.pay.java.service.payments.model.Transaction;
import com.wechat.pay.java.service.payments.nativepay.NativePayService;
import com.wechat.pay.java.service.payments.nativepay.model.Amount;
import com.wechat.pay.java.service.payments.nativepay.model.PrepayRequest;
import com.wechat.pay.java.service.payments.nativepay.model.PrepayResponse;
import com.wechat.pay.java.service.payments.nativepay.model.QueryOrderByOutTradeNoRequest;
import com.wechat.pay.java.service.refund.RefundService;
import com.wechat.pay.java.service.refund.model.AmountReq;
import com.wechat.pay.java.service.refund.model.CreateRequest;
import com.wechat.pay.java.service.refund.model.Refund;
import com.wechat.pay.java.service.refund.model.Status;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.HashMap;

@Service
@Slf4j
public class WxPayServiceImpl implements WxPayService {
    @Autowired
    private RSAAutoCertificateConfig rsaAutoCertificateConfig;

    @Autowired
    private WxPayConfig wxPayConfig;

    @Autowired
    private OrderInfoService orderInfoService;
    @Autowired
    private RefundInfoService refundInfoService;

    @Override
    public String createNative(String outTradeNo) {

        // 初始化服务
        NativePayService service = new NativePayService.Builder().config(rsaAutoCertificateConfig).build();

        // 调用接口
        try {

            //获取订单
            OrderInfo orderInfo = orderInfoService.selectByOutTradeNo(outTradeNo);

            PrepayRequest request = new PrepayRequest();
            // 调用request.setXxx(val)设置所需参数，具体参数可见Request定义
            request.setAppid(wxPayConfig.getAppid());
            request.setMchid(wxPayConfig.getMchId());

            request.setDescription(orderInfo.getTitle());
            request.setOutTradeNo(outTradeNo);
            request.setNotifyUrl(wxPayConfig.getNotifyUrl());

            Amount amount = new Amount();
            //amount.setTotal(orderInfo.getAmount().multiply(new BigDecimal(100)).intValue());
            amount.setTotal(1);//1分钱
            request.setAmount(amount);
            // 调用接口
            PrepayResponse prepayResponse = service.prepay(request);

            return prepayResponse.getCodeUrl();

        } catch (HttpException e) { // 发送HTTP请求失败
            // 调用e.getHttpRequest()获取请求打印日志或上报监控，更多方法见HttpException定义
            log.error(e.getHttpRequest().toString());
            throw new GuiguException(ResultCodeEnum.FAIL);
        } catch (ServiceException e) { // 服务返回状态小于200或大于等于300，例如500
            // 调用e.getResponseBody()获取返回体打印日志或上报监控，更多方法见ServiceException定义
            log.error(e.getResponseBody());
            throw new GuiguException(ResultCodeEnum.FAIL);
        } catch (MalformedMessageException e) { // 服务返回成功，返回体类型不合法，或者解析返回体失败
            // 调用e.getMessage()获取信息打印日志或上报监控，更多方法见MalformedMessageException定义
            log.error(e.getMessage());
            throw new GuiguException(ResultCodeEnum.FAIL);
        }
    }

    @Autowired
    private PaymentInfoService paymentInfoService;

    @Override
    public boolean queryPayStatus(String outTradeNo) {

        // 初始化服务
        NativePayService service = new NativePayService.Builder().config(rsaAutoCertificateConfig).build();

        // 调用接口
        try {

            QueryOrderByOutTradeNoRequest request = new QueryOrderByOutTradeNoRequest();
            // 调用request.setXxx(val)设置所需参数，具体参数可见Request定义
            request.setOutTradeNo(outTradeNo);
            request.setMchid(wxPayConfig.getMchId());

            // 调用接口
            Transaction transaction = service.queryOrderByOutTradeNo(request);
            Transaction.TradeStateEnum tradeState = transaction.getTradeState();

            //支付成功
            if (tradeState.equals(Transaction.TradeStateEnum.SUCCESS)) {

                OrderInfo orderInfo = orderInfoService.selectByOutTradeNo(outTradeNo);
//处理支付成功后重复查单
//保证接口调用的幂等性：无论接口被调用多少次，产生的结果是一致的
                Integer orderStatus = orderInfo.getOrderStatus();
                if (OrderStatusEnum.UNPAID.getStatus().intValue() != orderStatus.intValue()) {
                    return true;//支付成功、关单、。。。
                }
                //更新订单状态
                orderInfoService.updateStatus(outTradeNo, OrderStatusEnum.PAID.getStatus());
                //记录支付日志
                paymentInfoService.savePaymentInfo(orderInfo, transaction);

                //通知医院修改订单状态
                //组装参数
                HashMap<String, Object> paramsMap = new HashMap<>();
                paramsMap.put("hoscode", orderInfo.getHoscode());
                paramsMap.put("hosOrderId", orderInfo.getHosOrderId());
                paramsMap.put("timestamp", HttpRequestHelper.getTimestamp());

                paramsMap.put("sign", HttpRequestHelper.getSign(paramsMap, "8af52af00baf6aec434109fc17164aae"));
                //发送请求
                JSONObject jsonResult = HttpRequestHelper.sendRequest(paramsMap, "http://localhost:9998/order/updatePayStatus");
                //解析响应
                if (jsonResult.getInteger("code") != 200) {
                    log.error("查单失败，"
                            + "code：" + jsonResult.getInteger("code")
                            + "，message：" + jsonResult.getString("message")
                    );
                    throw new GuiguException(ResultCodeEnum.FAIL.getCode(), jsonResult.getString("message"));
                }

                //返回支付结果
                return true;//支付成功
            }

            return false;//支付中

        } catch (HttpException e) { // 发送HTTP请求失败
            // 调用e.getHttpRequest()获取请求打印日志或上报监控，更多方法见HttpException定义
            log.error(e.getHttpRequest().toString());
            throw new GuiguException(ResultCodeEnum.FAIL);
        } catch (ServiceException e) { // 服务返回状态小于200或大于等于300，例如500
            // 调用e.getResponseBody()获取返回体打印日志或上报监控，更多方法见ServiceException定义
            log.error(e.getResponseBody());
            throw new GuiguException(ResultCodeEnum.FAIL);
        } catch (MalformedMessageException e) { // 服务返回成功，返回体类型不合法，或者解析返回体失败
            // 调用e.getMessage()获取信息打印日志或上报监控，更多方法见MalformedMessageException定义
            log.error(e.getMessage());
            throw new GuiguException(ResultCodeEnum.FAIL);
        }
    }


    @Override
    public void refund(String outTradeNo) {
//微信退款申请提交
        // 初始化服务
        RefundService service = new RefundService.Builder().config(rsaAutoCertificateConfig).build();

        // 调用接口
        try {

            //获取订单
            OrderInfo orderInfo = orderInfoService.selectByOutTradeNo(outTradeNo);

            CreateRequest request = new CreateRequest();
            // 调用request.setXxx(val)设置所需参数，具体参数可见Request定义
            request.setOutTradeNo(outTradeNo);
            request.setOutRefundNo("TK_" + outTradeNo);
            AmountReq amount = new AmountReq();
            //amount.setTotal(orderInfo.getAmount().multiply(new BigDecimal(100)).intValue());
            amount.setTotal(1L);//1分钱
            amount.setRefund(1L);
            amount.setCurrency("CNY");
            request.setAmount(amount);
            request.setNotifyUrl(wxPayConfig.getNotifyRefundUrl());
            //5退款服务发送请求拿响应
            Refund response = service.create(request);
//6响应中获取状态判断提交申请失败情况
            Status status = response.getStatus();

            //            SUCCESS：退款成功（退款申请成功）
            //            CLOSED：退款关闭
            //            PROCESSING：退款处理中
            //            ABNORMAL：退款异常
            if(Status.CLOSED.equals(status)){

                throw new GuiguException(ResultCodeEnum.FAIL.getCode(), "退款已关闭，无法退款");

            }else if(Status.ABNORMAL.equals(status)){

                throw new GuiguException(ResultCodeEnum.FAIL.getCode(), "退款异常");

            } else{
                //SUCCESS：退款成功（退款申请成功） || PROCESSING：退款处理中
                //记录支退款日志
                refundInfoService.saveRefundInfo(orderInfo, response);
            }

        } catch (HttpException e) { // 发送HTTP请求失败
            // 调用e.getHttpRequest()获取请求打印日志或上报监控，更多方法见HttpException定义
            log.error(e.getHttpRequest().toString());
            throw new GuiguException(ResultCodeEnum.FAIL);
        } catch (ServiceException e) { // 服务返回状态小于200或大于等于300，例如500
            // 调用e.getResponseBody()获取返回体打印日志或上报监控，更多方法见ServiceException定义
            log.error(e.getResponseBody());
            throw new GuiguException(ResultCodeEnum.FAIL);
        } catch (MalformedMessageException e) { // 服务返回成功，返回体类型不合法，或者解析返回体失败
            // 调用e.getMessage()获取信息打印日志或上报监控，更多方法见MalformedMessageException定义
            log.error(e.getMessage());
            throw new GuiguException(ResultCodeEnum.FAIL);
        }
    }
}
