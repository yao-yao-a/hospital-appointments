package com.atguigu.syt.order.Scheduled;

import com.atguigu.syt.order.service.OrderInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
@EnableScheduling  //开启定时任务
@Slf4j
public class ScheduledTask {
//    /**
//     * 测试
//     * (cron="秒 分 时 日 月 周")
//     * *：每隔一秒执行
//     * 0/3：从第0秒开始，每隔3秒执行一次
//     * 1-3: 从第1秒开始执行，到第3秒结束执行
//     * 1,2,3：第1、2、3秒执行
//     * ?：不指定，若指定日期，则不指定周，反之同理
//     */
//    @Scheduled(cron="0/3 * * * * ?")
//    public void task1() {
//        log.info("task1 执行");
//    }
    @Autowired
    private OrderInfoService orderInfoService;

    @Scheduled(cron = "0 0 18 * * ?")
    public void patientAdviceTask(){

        log.info("执行定时任务");
        orderInfoService.patientAdvice();
    }
}
