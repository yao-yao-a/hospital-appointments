package com.atguigu.syt.order.service;

public interface WxPayService {
    /**
     * 获取支付二维码utl
     * @param outTradeNo
     * @return
     */
    String createNative(String outTradeNo);
    /**
     * 查询订单支付状态
     * @param outTradeNo
     * @return
     */
    boolean queryPayStatus(String outTradeNo);
    /**
     * 退款
     * @param outTradeNo
     */
    void refund(String outTradeNo);
}
