package com.atguigu.syt.order.service;


import com.atguigu.syt.model.order.OrderInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author atguigu
 * @since 2023-07-24
 */
public interface OrderInfoService extends IService<OrderInfo> {
    /**
     * 根据排班id和就诊人id创建订单
     * @param scheduleId
     * @param patientId
     * @return 新订单id
     */
    Long submitOrder(String scheduleId, Long patientId);
    /**
     * 根据订单id获取订单详情
     * @param orderId
     * @return
     */
    OrderInfo getOrderInfo(Long orderId);
    /**
     * 获取当前用户订单列表
     * @param userId
     * @return
     */
    List<OrderInfo> selectList(Long userId);
    /**
     * 根据订单号获取订单
     * @param outTradeNo
     * @return
     */
    OrderInfo selectByOutTradeNo(String outTradeNo);
    /**
     * 根据订单号更新订单状态
     * @param outTradeNo
     * @param status
     */
    void updateStatus(String outTradeNo, Integer status);
    /**
     * 根据订单号取消订单
     * @param outTradeNo
     */
    void cancelOrder(String outTradeNo);
    /**
     * 就诊人提醒
     */
    void patientAdvice();
}
