package com.atguigu.syt.user.controller.front;

import com.atguigu.common.service.exception.GuiguException;
import com.atguigu.common.util.result.Result;
import com.atguigu.common.util.result.ResultCodeEnum;
import com.atguigu.syt.model.user.UserInfo;
import com.atguigu.syt.user.utils.ConstantProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.net.URLEncoder;
import java.util.concurrent.ThreadLocalRandom;

@Api(tags = "微信扫码登录")
@Controller//注意这里没有配置 @RestController
@RequestMapping("/front/user/wx")
@Slf4j
public class FrontWxController {
    @Autowired
    private ConstantProperties constantProperties;

    @GetMapping("login")
    public String login(HttpSession session){
        try {

            StringBuffer baseUrl = new StringBuffer()
                    .append("https://open.weixin.qq.com/connect/qrconnect")
                    .append("?appid=%s")
                    .append("&redirect_uri=%s")
                    .append("&response_type=code")
                    .append("&scope=snsapi_login")
                    .append("&state=%s")
                    .append("#wechat_redirect");


            //处理回调url
            String  redirectUri = URLEncoder.encode(constantProperties.getRedirectUri(), "UTF-8");
            //处理state：生成随机数，存入session
            //ThreadLocalRandom解决了Random在高并发环境下随机数生成性能问题
            long nonce = ThreadLocalRandom.current().nextLong(Long.MAX_VALUE);
            //十六进制表示的随机数
            String state = Long.toHexString(nonce);
            log.info("生成 state = " + state);
            session.setAttribute("wx_open_state", state);

            String qrcodeUrl = String.format(
                    baseUrl.toString(),
                    constantProperties.getAppId(),
                    redirectUri,
                    state
            );

            return "redirect:" + qrcodeUrl;

        } catch (Exception e) {
            throw new GuiguException(ResultCodeEnum.URL_ENCODE_ERROR, e);
        }
    }

}
