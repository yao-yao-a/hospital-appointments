package com.atguigu.syt.user.service.impl;

import com.atguigu.common.service.exception.GuiguException;
import com.atguigu.common.util.result.ResultCodeEnum;
import com.atguigu.syt.cmn.client.DictFeignClient;
import com.atguigu.syt.enums.AuthStatusEnum;
import com.atguigu.syt.enums.DictTypeEnum;
import com.atguigu.syt.enums.UserStatusEnum;
import com.atguigu.syt.model.user.Patient;
import com.atguigu.syt.model.user.UserInfo;
import com.atguigu.syt.user.mapper.UserInfoMapper;
import com.atguigu.syt.user.service.PatientService;
import com.atguigu.syt.user.service.UserInfoService;
import com.atguigu.syt.vo.user.UserAuthVo;
import com.atguigu.syt.vo.user.UserInfoQueryVo;
import com.atguigu.syt.yun.client.FileFeignClient;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author atguigu
 * @since 2023-07-18
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {
    @Autowired
    private DictFeignClient dictFeignClient;
    @Autowired
    private FileFeignClient fileFeignClient;
    @Autowired
    private PatientService patientService;

    @Override
    public UserInfo getByOpenId(String openid) {

        LambdaQueryWrapper<UserInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserInfo::getOpenid, openid);
        return baseMapper.selectOne(queryWrapper);
    }

    @Override
    public UserInfo getUserInfoById(Long userId) {
        UserInfo userInfo = baseMapper.selectById(userId);
        return this.packUserInfo(userInfo);
    }

    /**
     * 封装用户状态、认证状态、证件类型信息
     *
     * @param userInfo
     * @return
     */
    private UserInfo packUserInfo(UserInfo userInfo) {
        //翻译证件类型
        String certificatesTypeString =
                dictFeignClient.getName(DictTypeEnum.CERTIFICATES_TYPE.getDictTypeId(),
                        userInfo.getCertificatesType());

        //翻译认证状态
        String authStatusString = AuthStatusEnum.getStatusNameByStatus(
                userInfo.getAuthStatus());

        String statusNameByStatus = UserStatusEnum.getStatusNameByStatus(userInfo.getStatus());

        userInfo.getParam().put("certificatesTypeString", certificatesTypeString);

        userInfo.getParam().put("authStatusString", authStatusString);

        userInfo.getParam().put(
                "statusString", statusNameByStatus);
//获取查看图片地址
        if (!StringUtils.isEmpty(userInfo.getCertificatesUrl())) {
            String previewUrl = fileFeignClient.getPreviewUrl(userInfo.getCertificatesUrl());
            userInfo.getParam().put("previewUrl", previewUrl);
        }
        return userInfo;
    }

    @Override
    public void userAuth(Long userId, UserAuthVo userAuthVo) {
        //用户认证
        //1根据用户id查询用户信息
        UserInfo userInfo = baseMapper.selectById(userId);
        //2把userAuthVo存入用户信息，补全用户认证状态
        BeanUtils.copyProperties(userAuthVo, userInfo);
        userInfo.setAuthStatus(AuthStatusEnum.AUTH_RUN.getStatus());
        //3更新数据
        baseMapper.updateById(userInfo);
    }

    @Override
    public IPage<UserInfo> selectPage(Page<UserInfo> pageParam, UserInfoQueryVo userInfoQueryVo) {
        //UserInfoQueryVo获取条件值
        String keyword = userInfoQueryVo.getKeyword(); //用户名称
        String createTimeBegin = userInfoQueryVo.getCreateTimeBegin(); //开始时间
        String createTimeEnd = userInfoQueryVo.getCreateTimeEnd(); //结束时间
        //对条件值进行非空判断,封装查询条件
        LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();

        //  wrapper.and(!StringUtils.isEmpty(keyword),
        //           i -> i.like(UserInfo::getName, keyword).or().like(UserInfo::getPhone, keyword))
        //   .ge(!StringUtils.isEmpty(createTimeBegin), UserInfo::getCreateTime, createTimeBegin)
        //      .le(!StringUtils.isEmpty(createTimeEnd), UserInfo::getCreateTime, createTimeEnd);
        wrapper.and(!StringUtils.isEmpty(keyword), new Consumer<LambdaQueryWrapper<UserInfo>>() {
                    @Override
                    public void accept(LambdaQueryWrapper<UserInfo> i) {
                        i.like(UserInfo::getName, keyword).or().like(UserInfo::getPhone, keyword)
                                .ge(!StringUtils.isEmpty(createTimeBegin), UserInfo::getCreateTime, createTimeBegin)
                                .le(!StringUtils.isEmpty(createTimeEnd), UserInfo::getCreateTime, createTimeEnd);

                    }
                }
        );

        //调用mapper的方法
        IPage<UserInfo> pages = baseMapper.selectPage(pageParam, wrapper);
        //编号变成对应值封装
        List<UserInfo> userInfoList = pages.getRecords();
        for (UserInfo userInfo : userInfoList) {
            this.packUserInfoForList(userInfo);
        } //pages.getRecords().forEach(this::packUserInfoForList);
        return pages;
    }

    /**
     * 封装用户状态、认证状态、证件类型信息
     *
     * @param userInfo
     * @return
     */
    private UserInfo packUserInfoForList(UserInfo userInfo) {

        //判断用户是否已经提交实名认证
        if (userInfo.getAuthStatus().intValue() != AuthStatusEnum.NO_AUTH.getStatus().intValue()) {
            //翻译证件类型
            String certificatesTypeString = dictFeignClient.getName(
                    DictTypeEnum.CERTIFICATES_TYPE.getDictTypeId(),
                    userInfo.getCertificatesType()
            );
            userInfo.getParam().put("certificatesTypeString", certificatesTypeString);
        }

        userInfo.getParam().put("authStatusString", AuthStatusEnum.getStatusNameByStatus(userInfo.getAuthStatus()));
        userInfo.getParam().put("statusString", UserStatusEnum.getStatusNameByStatus(userInfo.getStatus()));

        return userInfo;
    }

    @Override
    public boolean lock(Long userId, Integer status) {
        if (status == 0 || status == 1) {
            UserInfo userInfo = new UserInfo();
            userInfo.setId(userId);
            userInfo.setStatus(status);
            return this.updateById(userInfo);
        }
        return false;
    }

    @Override
    public Map<String, Object> show(Long userId) {
        Map<String, Object> map = new HashMap<>();
        //根据userid查询用户信息
        UserInfo userInfo = this.packUserInfo(baseMapper.selectById(userId));
        map.put("userInfo", userInfo);

        //根据userid查询就诊人信息
        List<Patient> patientList = patientService.findByUserId(userId);
        map.put("patientList", patientList);
        return map;
    }

    /**
     * 认证审批  2通过  -1不通过
     *
     * @param userId
     * @param authStatus
     * @return
     */
    @Override
    public boolean approval(Long userId, Integer authStatus) {
        if (authStatus == AuthStatusEnum.AUTH_SUCCESS.getStatus()
                || authStatus == AuthStatusEnum.AUTH_FAIL.getStatus()) {
            UserInfo userInfo = new UserInfo();
            userInfo.setId(userId);
            userInfo.setAuthStatus(authStatus);
            return this.updateById(userInfo);
        }
        return false;
    }

    @Resource
    private RedisTemplate redisTemplate;

    @Override
    public void bindPhone(Long userId, String phone, String code) {

        //校验参数
        if (StringUtils.isEmpty(phone) || StringUtils.isEmpty(code)) {
            throw new GuiguException(ResultCodeEnum.PARAM_ERROR);
        }

        //校验验证码
        String phoneCode = (String) redisTemplate.opsForValue().get("code:" + phone);
        if (!code.equals(phoneCode)) {
            throw new GuiguException(ResultCodeEnum.CODE_ERROR);
        }

        //根据手机号查找会员
        LambdaQueryWrapper<UserInfo> queryWrapper = new LambdaQueryWrapper<>();
        //手机号没有被其他人绑定过
        queryWrapper.eq(UserInfo::getPhone, phone).ne(UserInfo::getId, userId);
        UserInfo userInfo = baseMapper.selectOne(queryWrapper);

        //手机号已存在
        if (userInfo != null) {
            throw new GuiguException(ResultCodeEnum.REGISTER_MOBILE_ERROR);
        }
        //设置绑定手机号
        userInfo = new UserInfo();
        userInfo.setId(userId);
        userInfo.setPhone(phone);
        baseMapper.updateById(userInfo);
    }
}
