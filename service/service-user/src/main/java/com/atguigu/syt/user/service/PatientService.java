package com.atguigu.syt.user.service;

import com.atguigu.syt.model.user.Patient;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 就诊人表 服务类
 * </p>
 *
 * @author atguigu
 * @since 2023-07-18
 */
public interface PatientService extends IService<Patient> {
    /**
     * 根据id获取本人名下的就诊人信息
     * @param id
     * @param userId
     * @return
     */
    Patient getPatientById(Long id, Long userId);
    /**
     * 根据userId获取就诊人列表
     * @param userId
     * @return
     */
    List<Patient> findByUserId(Long userId);
    /**
     * 根据id删除自己名下的就诊人信息
     * @param id
     * @param userId
     */
    void removePatient(Long id, Long userId);
}
