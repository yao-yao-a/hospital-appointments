package com.atguigu.syt.user.controller.front;

import com.atguigu.common.service.utils.AuthContextHolder;
import com.atguigu.common.util.result.Result;
import com.atguigu.syt.model.user.Patient;
import com.atguigu.syt.user.service.PatientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Api(tags = "就诊人管理")
@RestController
@RequestMapping("/front/user/patient")
public class FrontPatientController {

    @Resource
    private PatientService patientService;

    @Resource
    private AuthContextHolder authContextHolder;

    @ApiOperation("添加就诊人")
    @ApiImplicitParam(name = "patient", value = "就诊人对象", required = true)
    @PostMapping("/auth/save")
    public Result savePatient(@RequestBody Patient patient, HttpServletRequest request, HttpServletResponse response) {
        //1校验登录，获取userId 
        Long userId = authContextHolder.checkAuth(request, response);
        //2把userId存入patient
        patient.setUserId(userId);
        //3调用接口，保存就诊人
        patientService.save(patient);

        return Result.ok().message("保存成功");
    }
    @ApiOperation("修改就诊人")
    @ApiImplicitParam(name = "patient",value = "就诊人对象", required = true)
    @PutMapping("/auth/update")
    public Result updatePatient(@RequestBody Patient patient, HttpServletRequest request, HttpServletResponse response) {
        //1校验登录
        authContextHolder.checkAuth(request, response);
        //3调用接口，保存就诊人
        patientService.updateById(patient);
        return Result.ok().message("修改成功");
    }
    @ApiOperation("根据id获取就诊人信息")
    @ApiImplicitParam(name = "id",value = "就诊人id", required = true)
    @GetMapping("/auth/get/{id}")
    public Result<Patient> getPatient(@PathVariable Long id, HttpServletRequest request, HttpServletResponse response) {

        Long userId = authContextHolder.checkAuth(request, response);
        //加上userId参数，只可以获取自己名下的就诊人信息
        Patient patient = patientService.getPatientById(id, userId);
        return Result.ok(patient);
    }
    @ApiOperation("获取就诊人列表")
    @GetMapping("/auth/findAll")
    public Result<List<Patient>> findAll(HttpServletRequest request, HttpServletResponse response) {
        //1校验登录，获取userId
        Long userId = authContextHolder.checkAuth(request, response);
        //2调用接口，根据userId获取就诊人列表
        List<Patient> list = patientService.findByUserId(userId);
        return Result.ok(list);
    }
    @ApiOperation("删除就诊人")
    @DeleteMapping("/auth/remove/{id}")
    public Result removePatient(@PathVariable Long id, HttpServletRequest request, HttpServletResponse response) {
//1校验登录，获取userId
        Long userId = authContextHolder.checkAuth(request, response);
        //加上userId参数，只可以删除自己名下的就诊人信息
        //2调用接口，根据id、userId删除就诊人
        patientService.removePatient(id, userId);
        return Result.ok();
    }
}