package com.atguigu.syt.user.controller.front;

import com.atguigu.common.service.utils.AuthContextHolder;
import com.atguigu.common.util.result.Result;
import com.atguigu.syt.model.user.UserInfo;
import com.atguigu.syt.user.service.UserInfoService;
import com.atguigu.syt.vo.user.UserAuthVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Api(tags = "用户管理")
@RestController
@RequestMapping("/front/user/userInfo")
public class FrontUserInfoController {

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private AuthContextHolder authContextHolder;

    @ApiOperation(value = "用户认证")
    @ApiImplicitParam(name = "userAuthVo", value = "用户实名认证对象", required = true)
    @PostMapping("/auth/userAuth")
    public Result userAuth(@RequestBody UserAuthVo userAuthVo, HttpServletRequest request, HttpServletResponse response) {
        //1登录校验，获取用户id
        Long userId = authContextHolder.checkAuth(request, response);
        //2调用接口，实现用户认证
        userInfoService.userAuth(userId, userAuthVo);
        return Result.ok();
    }

    @ApiOperation(value = "获取认证信息")
    @GetMapping("/auth/getUserInfo")
    public Result<UserInfo> getUserInfo(HttpServletRequest request, HttpServletResponse response) {
        //1登录校验，获取用户id
        Long userId = authContextHolder.checkAuth(request, response);
        //2根据userid查询用户信息
        UserInfo userInfo = userInfoService.getUserInfoById(userId);
        return Result.ok(userInfo);
    }

    @ApiOperation("绑定手机号")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "手机号", required = true),
            @ApiImplicitParam(name = "code", value = "验证码", required = true)})
    @PostMapping("/auth/bindPhone/{phone}/{code}")
    public Result bindPhone(@PathVariable String phone, @PathVariable String code, HttpServletRequest request, HttpServletResponse response) {

        Long userId = authContextHolder.checkAuth(request, response);
        userInfoService.bindPhone(userId, phone, code);
        return Result.ok().message("绑定成功");
    }

}
