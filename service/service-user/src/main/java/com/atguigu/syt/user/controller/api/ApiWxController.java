package com.atguigu.syt.user.controller.api;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.common.service.exception.GuiguException;
import com.atguigu.common.service.utils.AuthContextHolder;
import com.atguigu.common.service.utils.HttpUtil;
import com.atguigu.common.util.result.ResultCodeEnum;
import com.atguigu.syt.enums.UserStatusEnum;
import com.atguigu.syt.model.user.UserInfo;
import com.atguigu.syt.user.service.UserInfoService;
import com.atguigu.syt.user.utils.ConstantProperties;
import com.atguigu.syt.vo.user.UserVo;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.URLEncoder;

@Api(tags = "微信扫码登录回调")
@Controller//注意这里没有配置 @RestController
@RequestMapping("/api/user/wx")
@Slf4j
public class ApiWxController {
    @Autowired
    private ConstantProperties constantProperties;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private AuthContextHolder authContextHolder;

    /**
     * 登录回调
     * @param code
     * @param state
     * @param session
     * @return
     */
    @GetMapping("callback")
    public String callback(String code, String state, HttpSession session, HttpServletResponse response) {
        try {
            //得到授权临时票据code和state参数
            log.info("callback被调用");
            log.info("code = " + code);
            log.info("state = " + state);
            String sessionState = (String) session.getAttribute("wx_open_state");
            log.info("sessionState = " + sessionState);
            log.info("seesion_id = " + session.getId());

            if (StringUtils.isEmpty(code) || StringUtils.isEmpty(state) || !state.equals(sessionState)) {
                throw new GuiguException(ResultCodeEnum.ILLEGAL_CALLBACK_REQUEST_ERROR);
            }

            //使用code和appid以及appscrect换取access_token
            StringBuffer baseAccessTokenUrl = new StringBuffer()
                    .append("https://api.weixin.qq.com/sns/oauth2/access_token")
                    .append("?appid=%s")
                    .append("&secret=%s")
                    .append("&code=%s")
                    .append("&grant_type=authorization_code");

            String accessTokenUrl = String.format(baseAccessTokenUrl.toString(),
                    constantProperties.getAppId(),
                    constantProperties.getAppSecret(),
                    code);

            //使用httpclient发送请求
            byte[] respdata = HttpUtil.doGet(accessTokenUrl);
            String result = new String(respdata);
            log.info("accesstokenInfo：" + result);

            JSONObject resultJson = JSONObject.parseObject(result);
            if (resultJson.getString("errcode") != null) {
                log.error("获取access_token失败：" + resultJson.getString("errcode") + resultJson.getString("errmsg"));
                throw new GuiguException(ResultCodeEnum.FETCH_ACCESSTOKEN_FAILD);
            }

            String accessToken = resultJson.getString("access_token");
            String openId = resultJson.getString("openid");
            log.info(accessToken);
            log.info(openId);

            //根据access_token获取微信用户的基本信息
            //先根据openid进行数据库查询
            UserInfo userInfo = userInfoService.getByOpenId(openId);

            if (userInfo != null) { //存在
                log.info("判断用户是否被禁用");
                if(userInfo.getStatus() == UserStatusEnum.LOCK.getStatus()){
                    log.error("用户已被禁用");
                    throw new GuiguException(ResultCodeEnum.LOGIN_DISABLED_ERROR);
                }
            }else{

                log.info("注册用户");
                //使用access_token换取受保护的资源：微信的个人信息
                String baseUserInfoUrl = "https://api.weixin.qq.com/sns/userinfo" +
                        "?access_token=%s" +
                        "&openid=%s";
                //使用httpclient发送请求
                String userInfoUrl = String.format(baseUserInfoUrl, accessToken, openId);
                byte[] respdataUser = HttpUtil.doGet(userInfoUrl);
                String resultUserInfo = new String(respdataUser);

                JSONObject resultUserInfoJson = JSONObject.parseObject(resultUserInfo);
                if (resultUserInfoJson.getString("errcode") != null) {
                    log.error("获取用户信息失败：" + resultUserInfoJson.getString("errcode") + resultUserInfoJson.getString("errmsg"));
                    throw new GuiguException(ResultCodeEnum.FETCH_USERINFO_ERROR);
                }

                //解析用户信息
                String nickname = resultUserInfoJson.getString("nickname");
                String headimgurl = resultUserInfoJson.getString("headimgurl");


                //用户注册
                userInfo = new UserInfo();
                userInfo.setOpenid(openId);
                userInfo.setNickName(nickname);
                userInfo.setHeadImgUrl(headimgurl);
                userInfo.setStatus(UserStatusEnum.NORMAL.getStatus());
                userInfoService.save(userInfo);

            }

            //获取用户名，如果没有用户名（未实名认证），则获取昵称
            String name = userInfo.getName();
            if (StringUtils.isEmpty(name)) {
                name = userInfo.getNickName();
            }

//            //生成token
//            String token = UUID.randomUUID().toString().replaceAll("-", "");
//            //将token做key，用户id做值存入redis
//            redisTemplate.opsForValue()//30分钟
//                    .set("user:token:" + token, userInfo.getId(), 30, TimeUnit.MINUTES);
//
//            //将token和name存入cookie
//            //将"资料>微信登录>CookieUtils.java"放入service-utils模块
//            int cookieMaxTime = 60 * 30;//30分钟
//            CookieUtils.setCookie(response, "token", token, cookieMaxTime);
//            CookieUtils.setCookie(response, "name", URLEncoder.encode(name), cookieMaxTime);
//            CookieUtils.setCookie(response, "headimgurl", URLEncoder.encode(userInfo.getHeadImgUrl()), cookieMaxTime);
            UserVo userVo = new UserVo();
            userVo.setName(name);
            userVo.setUserId(userInfo.getId());
            userVo.setHeadimgurl(userInfo.getHeadImgUrl());

            authContextHolder.saveToken(userVo, response);

            return "redirect:" + constantProperties.getSytBaseUrl();

        } catch (GuiguException e) {
            log.error(ExceptionUtils.getStackTrace(e));
            return "redirect:" + constantProperties.getSytBaseUrl()
                    + "?code=201&message=" + URLEncoder.encode(e.getMsg());
        }  catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            return "redirect:" + constantProperties.getSytBaseUrl()
                    + "?code=201&message="+URLEncoder.encode("登录失败");
        }
    }
}
