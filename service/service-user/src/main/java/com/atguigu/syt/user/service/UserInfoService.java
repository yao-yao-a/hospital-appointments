package com.atguigu.syt.user.service;

import com.atguigu.syt.model.user.UserInfo;
import com.atguigu.syt.vo.user.UserAuthVo;
import com.atguigu.syt.vo.user.UserInfoQueryVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author atguigu
 * @since 2023-07-18
 */
public interface UserInfoService extends IService<UserInfo> {
    /**
     * 根据openid查询用户信息
     * @param openid
     * @return
     */
    UserInfo getByOpenId(String openid);
    /**
     * 根据用户id获取用户信息
     * @param userId
     * @return
     */
    UserInfo getUserInfoById(Long userId);
    /**
     * 保存实名认证信息
     * @param userId
     * @param userAuthVo
     */
    void userAuth(Long userId, UserAuthVo userAuthVo);
    /**
     * 查询用户分页列表
     * @param pageParam
     * @param userInfoQueryVo
     * @return
     */
    IPage<UserInfo> selectPage(Page<UserInfo> pageParam, UserInfoQueryVo userInfoQueryVo);
    /**
     * 锁定和解锁用户
     * @param userId
     * @param status
     * @return
     */
    boolean lock(Long userId, Integer status);
    /**
     * 根据用户id获取用户详情
     * @param userId
     * @return
     */
    Map<String, Object> show(Long userId);
    /**
     * 审核用户
     * @param userId
     * @param authStatus
     * @return
     */
    boolean approval(Long userId, Integer authStatus);
    /**
     * 绑定当前用户的手机号
     * @param userId
     * @param phone
     * @param code
     */
    void bindPhone(Long userId, String phone, String code);
}
