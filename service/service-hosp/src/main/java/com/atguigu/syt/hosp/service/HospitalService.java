package com.atguigu.syt.hosp.service;

import com.atguigu.syt.model.hosp.Hospital;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface HospitalService {
    /**
     * 保存医院信息
     *
     * @param paramMap
     */
    void save(Map<String, Object> paramMap);
    /**
     * 根据医院编码获取医院信息,转换为json数据
     * @param hoscode
     * @return
     */
    Hospital getByHoscode(String hoscode);
    /**
     * 根据医院名称分页查询医院列表
     * @param page
     * @param limit
     * @param hosname
     * @return
     */
    Page<Hospital> selectPage(Integer page, Integer limit, String hosname);
    /**
     * 根据医院编码修改医院状态
     * @param hoscode
     * @param status
     */
    void updateStatus(String hoscode, Integer status);
    /**
     * 根据医院编码获取医院详情
     * @param hoscode
     * @return
     */
    Hospital show(String hoscode);
    /**
     * 医院列表查询
     * @param hosname
     * @param hostype
     * @param districtCode
     * @return
     */
    List<Hospital> selectList(String hosname, String hostype, String districtCode);
}
