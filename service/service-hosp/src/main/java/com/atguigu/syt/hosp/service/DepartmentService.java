package com.atguigu.syt.hosp.service;

import com.atguigu.syt.model.hosp.Department;
import com.atguigu.syt.vo.hosp.DepartmentVo;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface DepartmentService {
    /**
     * 保存科室信息
     * @param paramMap
     */
    void save(Map<String, Object> paramMap);
    /**
     * 分页查询
     * @param page 当前页码
     * @param limit 每页记录数
     * @param hoscode 查询条件
     * @return
     */
    Page<Department> selectPage(int page, int limit, String hoscode);
    /**
     * 根据医院编码和科室编码删除科室
     * @param hoscode
     * @param depcode
     */
    void remove(String hoscode, String depcode);
    /**
     * 根据医院编码获取部门嵌套列表
     * @param hoscode
     * @return
     */
    List<DepartmentVo> findDeptTree(String hoscode);
}
