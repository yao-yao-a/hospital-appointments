package com.atguigu.syt.hosp.controller.admin;


import com.atguigu.common.util.result.Result;
import com.atguigu.common.util.tools.MD5;
import com.atguigu.syt.hosp.service.HospitalSetService;
import com.atguigu.syt.model.hosp.HospitalSet;
import com.atguigu.syt.vo.hosp.HospitalSetQueryVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

/**
 * <p>
 * 医院设置表 前端控制器
 * </p>
 *
 * @author atguigu
 * @since 2023-07-04
 */
@Api(tags = "医院设置管理")
@RestController
@RequestMapping("/admin/hosp/hospitalSet")
public class AdminHospitalSetController {

    @Autowired
    private HospitalSetService hospitalSetService;

    @ApiOperation(value = "根据id查询医院设置")
    @GetMapping("/getHospSet/{id}")
    public Result<HospitalSet> getById(
            @ApiParam(value = "医院设置id",required = true)
            @PathVariable Long id){
        HospitalSet hospitalSet = hospitalSetService.getById(id);
        return Result.ok(hospitalSet);
    }

    @ApiOperation(value = "根据id删除医院设置")
    @DeleteMapping("/{id}")
    public Result removeById(
            @ApiParam(value = "医院设置id",required = true)
            @PathVariable Long id){
        boolean result = hospitalSetService.removeById(id);
        if(result){
            return Result.ok().message("删除成功");
        }else{
            return Result.fail().message("删除失败");
        }
    }

    @ApiOperation(value = "新增医院设置")
    @PostMapping("/saveHospSet")
    public Result save(
            @ApiParam(value = "医院设置对象", required = true)
            @RequestBody HospitalSet hospitalSet){

        //设置状态 1可用 0锁定，1表示医院可以入驻尚易通平台
        hospitalSet.setStatus(1);
        //生成签名秘钥
        Random random = new Random();
        hospitalSet.setSignKey(MD5.encrypt(System.currentTimeMillis()+""+random.nextInt(1000)));
        boolean result = hospitalSetService.save(hospitalSet);
        if(result){
            return Result.ok().message("添加成功");
        }else{
            return Result.fail().message("添加失败");
        }
    }

    @ApiOperation(value = "根据ID修改医院设置")
    @PutMapping("/updateHospSet")
    public Result updateById(
            @ApiParam(value = "医院设置对象", required = true)
            @RequestBody HospitalSet hospitalSet){
        boolean result  = hospitalSetService.updateById(hospitalSet);
        if(result){
            return Result.ok().message("修改成功");
        }else{
            return Result.fail().message("修改失败");
        }
    }

    @ApiOperation(value = "批量删除医院设置") //[1,2,3]
    @DeleteMapping("/batchRemove")
    public Result batchRemoveHospitalSet(
            @ApiParam(value = "id列表", required = true)
            @RequestBody List<Long> idList) {

        boolean result = hospitalSetService.removeByIds(idList);
        if(result){
            return Result.ok().message("删除成功");
        }else{
            return Result.fail().message("删除失败");
        }
    }

    @ApiOperation(value = "医院设置锁定和解锁")
    @PutMapping("/lockHospitalSet/{id}/{status}")
    public Result lockHospitalSet(
            @ApiParam(value = "医院设置id",required = true) @PathVariable Long id,
            @ApiParam(value = "状态", required = true) @PathVariable Integer status) {

        if(status != 0 && status != 1){
            return Result.fail().message("非法数据");
        }

        HospitalSet hospitalSet = new HospitalSet();
        hospitalSet.setId(id);
        hospitalSet.setStatus(status);
        boolean result = hospitalSetService.updateById(hospitalSet);

        if(result){
            return Result.ok().message("操作成功");
        }else{
            return Result.fail().message("操作失败");
        }
    }

    /**
     * 分页查询
     */
    @ApiOperation("分页条件查询")
    @GetMapping("/{page}/{limit}")
    public Result<Page> pageList(
            @ApiParam(value = "页码",required = true)
            @PathVariable Long page,
            @ApiParam(value = "每页记录数",required = true)
            @PathVariable Long limit,
            @ApiParam(value = "查询对象",required = false)
            HospitalSetQueryVo hospitalSetQueryVo){
       Page<HospitalSet> pageList = hospitalSetService.selectPage(page,limit,hospitalSetQueryVo);
       return Result.ok(pageList);
    }


}