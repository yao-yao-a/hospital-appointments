package com.atguigu.syt.hosp.service.impl;

import com.alibaba.cloud.commons.lang.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.atguigu.common.service.exception.GuiguException;
import com.atguigu.common.util.result.ResultCodeEnum;
import com.atguigu.syt.cmn.client.DictFeignClient;
import com.atguigu.syt.cmn.client.RegionFeignClient;
import com.atguigu.syt.enums.DictTypeEnum;
import com.atguigu.syt.hosp.repository.HospitalRepository;
import com.atguigu.syt.hosp.service.HospitalService;
import com.atguigu.syt.model.hosp.Hospital;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class HospitalServiceImpl implements HospitalService {
    @Autowired
    private HospitalRepository hospitalRepository;
    @Autowired
    private DictFeignClient dictFeignClient;
    @Autowired
    private RegionFeignClient regionFeignClient;

    /**
     * 添加医院信息
     *
     * @param paramMap
     */
    @Override
    public void save(Map<String, Object> paramMap) {
        //1转化类型paramMap=》Hospital
        //1.1 paramMap=>json串
        String paramJsonStr = JSONObject.toJSONString(paramMap);
        //1.1 json串=>Hospital
        Hospital hospital = JSONObject.parseObject(paramJsonStr, Hospital.class);
        //2根据hoscode查询医院信息
        Hospital targetHospital = hospitalRepository.getByHoscode(hospital.getHoscode());

        if (targetHospital != null) {
            //3医院信息存在，更新数据
            hospital.setId(targetHospital.getId());
            hospital.setStatus(targetHospital.getStatus());
            hospitalRepository.save(hospital);
        } else {
            //4医院信息不存在，新增数据
            //0：未上线 1：已上线
            hospital.setStatus(1);
            hospitalRepository.save(hospital);
        }
    }

    @Override
    public Hospital getByHoscode(String hoscode) {
        return hospitalRepository.getByHoscode(hoscode);
    }

//    @Override
//    public Page<Hospital> selectPage(Integer page, Integer limit, String hosname) {
//
//        //1创建分页对象、设置排序
//        Sort sort = Sort.by(Sort.Direction.ASC, "hoscode");
//        //设置分页参数
//        PageRequest pageRequest = PageRequest.of(page - 1, limit, sort);
//
//        //2判断hosname是否为空
//        if (StringUtils.isEmpty(hosname)) {
//            //3hosname为空，实现分页查询
//            return hospitalRepository.findAll(pageRequest);
//        } else {
//            //4hosname不为空，调用新接口方法实现带条件带分页查询
//            return hospitalRepository.findByHosnameLike(hosname, pageRequest);
//        }
//    }
@Override
public Page<Hospital> selectPage(Integer page, Integer limit, String hosname) {

    //设置排序规则
    Sort sort = Sort.by(Sort.Direction.ASC, "hoscode");
    //设置分页参数
    PageRequest pageRequest = PageRequest.of(page-1, limit, sort);

    //执行查询
    Page<Hospital> pages = null;
    if(StringUtils.isEmpty(hosname)){
        pages = hospitalRepository.findAll(pageRequest);
    }else{
        pages = hospitalRepository.findByHosnameLike(hosname, pageRequest);
    }
//5从pageModel取出集合，遍历，翻译字段
    List<Hospital> content = pages.getContent();
    for (Hospital hospital : content) {
        this.packHospital(hospital);
    }
    return pages;
}
    /**
     * 封装Hospital数据
     * @param hospital
     * @return
     */
//翻译医院信息
    private Hospital packHospital(Hospital hospital) {
        //1翻译医院等级信息
        String hostypeString = dictFeignClient.getName(
                DictTypeEnum.HOSTYPE.getDictTypeId(), hospital.getHostype());
        //2翻译地域信息
        String provinceString = regionFeignClient.getName(hospital.getProvinceCode());
        String cityString = regionFeignClient.getName(hospital.getCityCode());
        if(provinceString.equals(cityString)){
            cityString = "";
        }
        String districtString = regionFeignClient.getName(hospital.getDistrictCode());
        hospital.getParam().put("hostypeString",hostypeString);
        hospital.getParam().put("fullAddress", provinceString + cityString + districtString + hospital.getAddress());
        return hospital;
    }
    //更新上线状态
    @Override
    public void updateStatus(String hoscode, Integer status) {
        if(status.intValue() == 0 || status.intValue() == 1) {
            Hospital hospital = hospitalRepository.findByHoscode(hoscode);
            hospital.setStatus(status);
            hospitalRepository.save(hospital);
            return;
        }

        throw new GuiguException(ResultCodeEnum.PARAM_ERROR);
    }
    @Override
    public Hospital show(String hoscode) {
        Hospital hospital = hospitalRepository.findByHoscode(hoscode);
        return this.packHospital(hospital);
    }
    /**
     * 根据条件查询医院列表
     * @param hosname 医院名称：模糊匹配
     * @param hostype：医院级别：精确匹配
     * @param districtCode：医院地区：精确匹配
     * @return
     */
    @Override
    public List<Hospital> selectList(String hosname, String hostype, String districtCode) {

        Sort sort = Sort.by(Sort.Direction.ASC, "hoscode");
        //List<Hospital> list = hospitalRepository.findByHosnameLikeAndHostypeAndDistrictCodeAndStatus(hosname, hostype, districtCode, 1, sort);

        //创建条件匹配器
        // GenericPropertyMatchers 通用属性匹配器
        ExampleMatcher matcher = ExampleMatcher.matching() //构建对象
                .withMatcher("hosname", ExampleMatcher.GenericPropertyMatchers.contains()) //模糊查询
                .withMatcher("hostype", ExampleMatcher.GenericPropertyMatchers.exact()) //精确查询
                .withMatcher("districtCode", ExampleMatcher.GenericPropertyMatchers.exact()); //精确查询
        //创建查询对象
        Hospital hospital = new Hospital();
        hospital.setHosname(hosname);
        hospital.setHostype(hostype);
        hospital.setDistrictCode(districtCode);
        hospital.setStatus(1); //已上线
        Example<Hospital> example = Example.of(hospital, matcher);
        //执行查询
        List<Hospital> list = hospitalRepository.findAll(example, sort);

        //封装医院等级数据
        //list.forEach(this::packHospital);
        for (Hospital hosp : list) {
            this.packHospital(hosp);
        }
        return list;
    }
}
