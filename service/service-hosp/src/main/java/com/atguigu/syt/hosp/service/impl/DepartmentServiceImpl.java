package com.atguigu.syt.hosp.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.syt.hosp.repository.DepartmentRepository;
import com.atguigu.syt.hosp.service.DepartmentService;
import com.atguigu.syt.model.hosp.Department;
import com.atguigu.syt.vo.hosp.DepartmentVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class DepartmentServiceImpl implements DepartmentService {
    @Autowired
    private DepartmentRepository departmentRepository;
    @Override
    public void save(Map<String, Object> paramMap) {

        // 把paramMap变成对象
        Department department = JSONObject.parseObject(JSONObject.toJSONString(paramMap), Department.class);

        //查询科室是否存在  医院编号 + 科室编号
        Department existsDepartment = departmentRepository.findByHoscodeAndDepcode(
                department.getHoscode(),
                department.getDepcode());

        //判断
        if(existsDepartment != null) { //修改
            department.setId(existsDepartment.getId());
            departmentRepository.save(department);
        } else {
            departmentRepository.save(department);
        }
    }
    @Override
    public Page<Department> selectPage(int page, int limit, String hoscode) {
        //设置排序规则
        Sort sort = Sort.by(Sort.Direction.ASC, "depcode");
        //设置分页参数
        //第一页从0开始
        Pageable pageable = PageRequest.of(page-1, limit, sort);
        //创建查询对象
        Department department = new Department();
        //封装查询条件
        department.setHoscode(hoscode);
        //创建查询条件模板
        Example<Department> example = Example.of(department);
        //带条件带分页查询
        Page<Department> pages = departmentRepository.findAll(example, pageable);
        return pages;
    }

    @Override
    public void remove(String hoscode, String depcode) {
        //根据医院编号、科室编号查询
        Department department = departmentRepository.findByHoscodeAndDepcode(hoscode, depcode);
        //判断查询出来数据，根据科室id删除
        if(department != null) {
            departmentRepository.deleteById(department.getId());
        }
    }
    @Override
    public List<DepartmentVo> findDeptTree(String hoscode) {

        //创建list集合，用于最终数据封装
        //1创建最终返回对象List<DepartmentVo>
        List<DepartmentVo> result = new ArrayList<>();
        //根据医院编号，查询医院所有科室信息
        //2根据hoscode查询所有关联科室集合List<Department>
        List<Department> departmentList = departmentRepository.findByHoscode(hoscode);
        //根据大科室编号  bigcode 分组，获取每个大科室里面下级子科室
        //collect：将流转换为其他形式。接收一个 Collector接口的实现，用于给Stream中元素做汇总的方法
        //3根据bigcode给List<Department>分组
        //List<Department>=>Map (k=bigcode ,v=List<Department>)
        Map<String, List<Department>> deparmentMap =
                departmentList.stream().collect(Collectors.groupingBy(Department::getBigcode));
        //4遍历Map进行大科室封装DepartmentVo
        for(Map.Entry<String,List<Department>> entry : deparmentMap.entrySet()) {
            //大科室编号
            String bigcode = entry.getKey();
            //大科室编号对应的全部数据
            List<Department> subList = entry.getValue();
            //封装大科室
            DepartmentVo departmentVo = new DepartmentVo();
            departmentVo.setDepcode(bigcode);
            departmentVo.setDepname(subList.get(0).getBigname());
            //封装小科室
            //5取出和大科室关联的小科室信息进行封装List<DepartmentVo>
            List<DepartmentVo> children = new ArrayList<>();
            for(Department subDepartment: subList) {
                DepartmentVo subDepartmentVo =  new DepartmentVo();
                subDepartmentVo.setDepcode(subDepartment.getDepcode());
                subDepartmentVo.setDepname(subDepartment.getDepname());
                //封装到list集合
                children.add(subDepartmentVo);
            }
            //把小科室list集合放到大科室children里面
            //6 封装小科室集合存入大科室对象
            departmentVo.setChildren(children);
            //放到最终result里面
            //7 把大科室对象DepartmentVo存入最终返回集合
            result.add(departmentVo);
        }
        //返回
        return result;
    }
}
