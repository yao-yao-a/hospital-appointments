package com.atguigu.syt.hosp.controller.front;

import com.atguigu.common.util.result.Result;
import com.atguigu.syt.hosp.service.ScheduleService;
import com.atguigu.syt.model.hosp.Schedule;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@Api(tags = "排班")
@RestController
@RequestMapping("/front/hosp/schedule")
public class FrontScheduleController {
    @Autowired
    private ScheduleService scheduleService;
    @ApiOperation(value = "获取可预约排班日期数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "hoscode",value = "医院编码", required = true),
            @ApiImplicitParam(name = "depcode",value = "科室编码", required = true)})
    @GetMapping("/getBookingScheduleRule/{hoscode}/{depcode}")
    public Result<Map<String,Object>>getBookingSchedule( @PathVariable String hoscode,
                                                         @PathVariable String depcode){
        Map<String, Object> result = scheduleService.getBookingScheduleRule(hoscode, depcode);
        return Result.ok(result);
    }
    @ApiOperation("获取排班数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "hoscode",value = "医院编码", required = true),
            @ApiImplicitParam(name = "depcode",value = "科室编码", required = true),
            @ApiImplicitParam(name = "workDate",value = "排班日期", required = true)})
    @GetMapping("getScheduleList/{hoscode}/{depcode}/{workDate}")
    public Result<List<Schedule>> getScheduleList(
            @PathVariable String hoscode,
            @PathVariable String depcode,
            @PathVariable String workDate) {
        List<Schedule> scheduleList = scheduleService.getScheduleList(hoscode, depcode, workDate);
        return Result.ok(scheduleList);
    }
    @ApiOperation("获取预约详情")
    @ApiImplicitParam(name = "id",value = "排班id", required = true)
    @GetMapping("getScheduleDetail/{id}")
    public Result<Schedule> getScheduleDetail(@PathVariable String id) {
        Schedule schedule = scheduleService.getDetailById(id);
        return Result.ok(schedule);
    }
}
