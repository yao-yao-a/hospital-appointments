package com.atguigu.syt.hosp.repository;

import com.atguigu.syt.model.hosp.Department;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface DepartmentRepository extends MongoRepository<Department, ObjectId> {
    //根据医院编号和科室编号查询科室
    Department findByHoscodeAndDepcode(String hoscode, String depcode);
    /**
     * 根据医院编号查询科室列表
     * @param hoscode
     * @return
     */
    List<Department> findByHoscode(String hoscode);
}
