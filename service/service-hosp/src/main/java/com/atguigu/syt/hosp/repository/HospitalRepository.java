package com.atguigu.syt.hosp.repository;

import com.atguigu.syt.model.hosp.Hospital;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface HospitalRepository extends MongoRepository<Hospital, ObjectId> {
    /**
     * 根据医院编号查询医院
     * @param hoscode
     * @return
     */
    Hospital getByHoscode(String hoscode);
    /**
     * 根据医院名称查询医院分页列表
     * @param hosname
     * @return
     */
    Page<Hospital> findByHosnameLike(String hosname, PageRequest pageRequest);

    Hospital findByHoscode(String hoscode);
}
