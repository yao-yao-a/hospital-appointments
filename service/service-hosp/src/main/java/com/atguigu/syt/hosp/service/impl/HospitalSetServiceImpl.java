package com.atguigu.syt.hosp.service.impl;

import com.atguigu.common.service.exception.GuiguException;
import com.atguigu.common.util.result.ResultCodeEnum;
import com.atguigu.syt.hosp.mapper.HospitalSetMapper;
import com.atguigu.syt.hosp.service.HospitalSetService;
import com.atguigu.syt.model.hosp.HospitalSet;
import com.atguigu.syt.vo.hosp.HospitalSetQueryVo;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 医院设置表 服务实现类
 * </p>
 *
 * @author atguigu
 * @since 2023-07-04
 */
@Service
public class HospitalSetServiceImpl extends ServiceImpl<HospitalSetMapper, HospitalSet> implements HospitalSetService {


    @Override
    public Page<HospitalSet> selectPage(Long page, Long limit, HospitalSetQueryVo hospitalSetQueryVo) {
        // 创建分页对象
        Page<HospitalSet> pageParam = new Page<>(page, limit);
        // 设置查询条件
        QueryWrapper<HospitalSet> queryWrapper = new QueryWrapper<>();
        // 获取医院的名称
        String hosname = hospitalSetQueryVo.getHosname();
        // 获取医院编号
        String hoscode = hospitalSetQueryVo.getHoscode();

        queryWrapper.like(!StringUtils.isEmpty(hosname), "hosname", hosname);
        queryWrapper.eq(!StringUtils.isEmpty(hoscode), "hoscode", hoscode);
        baseMapper.selectPage(pageParam, queryWrapper);
        return pageParam;
    }

    @Override
    public String getSignKey(String hoscode) {
        HospitalSet hospitalSet = this.getByHoscode(hoscode);
        if (hospitalSet == null) {
            throw new GuiguException(ResultCodeEnum.HOSPITAL_OPEN);
        }
        if (hospitalSet.getStatus().intValue() == 0) {
            throw new GuiguException(ResultCodeEnum.HOSPITAL_LOCK);
        }
        return hospitalSet.getSignKey();
    }

    @Override
    public HospitalSet getByHoscode(String hoscode) {
        LambdaQueryWrapper<HospitalSet> wrapper = new LambdaQueryWrapper<>();
        // 严格匹配，获取Hoscode，Hoscode是唯一的，所以直接selectOne
        wrapper.eq(HospitalSet::getHoscode, hoscode);
        HospitalSet hospitalSet = baseMapper.selectOne(wrapper);
        return hospitalSet;
    }
}
