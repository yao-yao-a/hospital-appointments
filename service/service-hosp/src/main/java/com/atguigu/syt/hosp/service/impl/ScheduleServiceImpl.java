package com.atguigu.syt.hosp.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.nacos.client.naming.utils.CollectionUtils;
import com.atguigu.syt.hosp.repository.DepartmentRepository;
import com.atguigu.syt.hosp.repository.HospitalRepository;
import com.atguigu.syt.hosp.repository.ScheduleRepository;
import com.atguigu.syt.hosp.service.ScheduleService;
import com.atguigu.syt.hosp.utils.DateUtil;
import com.atguigu.syt.model.hosp.BookingRule;
import com.atguigu.syt.model.hosp.Department;
import com.atguigu.syt.model.hosp.Hospital;
import com.atguigu.syt.model.hosp.Schedule;
import com.atguigu.syt.vo.hosp.BookingScheduleRuleVo;
import com.atguigu.syt.vo.hosp.ScheduleOrderVo;
import com.atguigu.syt.vo.hosp.ScheduleRuleVo;
import com.atguigu.syt.vo.order.OrderMqVo;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ScheduleServiceImpl implements ScheduleService {
    @Autowired
    private ScheduleRepository scheduleRepository;
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private HospitalRepository hospitalRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public void save(Map<String, Object> paramMap) {

        //数据转换
        Schedule schedule = JSONObject.parseObject(JSONObject.toJSONString(paramMap), Schedule.class);
        //查询mongodb中数据是否存在
        Schedule existSchedule = scheduleRepository.findByHoscodeAndHosScheduleId(
                schedule.getHoscode(),
                schedule.getHosScheduleId()
        );

        //如果不存在
        if (existSchedule != null) {//更新
            schedule.setId(existSchedule.getId());
            scheduleRepository.save(schedule);
        } else {//新增
            scheduleRepository.save(schedule);
        }
    }

    @Override
    public Page<Schedule> selectPage(int page, int limit, String hoscode) {
        //设置排序规则
        Sort sort = Sort.by(Sort.Direction.ASC, "workDate");
        //设置分页参数
        Pageable pageable = PageRequest.of(page - 1, limit, sort);
        //创建查询对象
        Schedule schedule = new Schedule();
        schedule.setHoscode(hoscode);
        Example<Schedule> example = Example.of(schedule);
        Page<Schedule> pages = scheduleRepository.findAll(example, pageable);
        return pages;
    }

    @Override
    public void remove(String hoscode, String hosScheduleId) {
        Schedule schedule = scheduleRepository.findByHoscodeAndHosScheduleId(hoscode, hosScheduleId);
        if (null != schedule) {
            scheduleRepository.deleteById(schedule.getId());
        }
    }


    @Override
    public Map<String, Object> getScheduleRule(Long page, Long limit, String hoscode, String depcode) {
//根据hoscode、depcode按排班日期统计号源信息（聚合查询）带分页，使用工具根据日期推算周几
        //获取查询结果
        //List<Schedule> list = mongoTemplate.findAll(Schedule.class);
        //查询条件：根据医院编号和科室编号查询
        Criteria criteria = Criteria.where("hoscode").is(hoscode).and("depcode").is(depcode);
        //根据工作日workDate期进行分组
        Aggregation agg = Aggregation.newAggregation(
                //1 查询条件
                Aggregation.match(criteria),
                Aggregation
                        //2 按照日期分组 select workDate as workDate from schedule group by workDate
                        .group("workDate").first("workDate").as("workDate")
                        //3 可预约数
                        .sum("reservedNumber").as("reservedNumber")
                        //4 剩余预约数
                        .sum("availableNumber").as("availableNumber"),
                //5 排序
                Aggregation.sort(Sort.Direction.ASC, "workDate"),
                //6 分页
                Aggregation.skip((page - 1) * limit),
                Aggregation.limit(limit)
        );
        //执行查询，查询Schedule集合，将文档中的同名字段数据存入BookingScheduleRuleVo类
//  执行聚合查询，获取结果
        AggregationResults<ScheduleRuleVo> aggResults = mongoTemplate.aggregate(agg, Schedule.class, ScheduleRuleVo.class);
        //获取查询结果
        List<ScheduleRuleVo> list = aggResults.getMappedResults();


        //分组查询后的总记录数
        //根据条件进行聚合查询 total
        Aggregation totalAgg = Aggregation.newAggregation(
                //设置筛选条件
                Aggregation.match(criteria),
                //指定分组字段，统计字段
                Aggregation.group("workDate")
        );
        AggregationResults<ScheduleRuleVo> totalAggResults = mongoTemplate.aggregate(totalAgg, Schedule.class, ScheduleRuleVo.class);
        int total = totalAggResults.getMappedResults().size();
        //根据日期计算对应星期获取 （iter）
        //遍历集合，换算周几封装数据
        for (ScheduleRuleVo scheduleRuleVo : list) {
            Date workDate = scheduleRuleVo.getWorkDate();
            String dayOfWeek = DateUtil.getDayOfWeek(new DateTime(workDate));
            scheduleRuleVo.setDayOfWeek(dayOfWeek);
        }
        //把数据存入map，返回数据
        Map<String, Object> result = new HashMap<>();
        result.put("list", list);
        result.put("total", total);
        return result;
    }



    /**
     * 根据日期对象和时间字符串获取一个日期时间对象
     *
     * @param dateTime
     * @param timeString
     * @return
     */
    private DateTime getDateTime(DateTime dateTime, String timeString) {
        String dateTimeString = dateTime.toString("yyyy-MM-dd") + " " + timeString;
        return DateTimeFormat.forPattern("yyyy-MM-dd HH:mm").parseDateTime(dateTimeString);
    }

    /**
     * 根据预约规则获取可预约日期列表
     */
    private List<Date> getDateList(BookingRule bookingRule) {
        //预约周期
        int cycle = bookingRule.getCycle();
        //当天放号时间
        DateTime releaseTime = this.getDateTime(new DateTime(), bookingRule.getReleaseTime());
        //如果当天放号时间已过，则预约周期后一天显示即将放号，周期加1
        if (releaseTime.isBeforeNow()) {
            cycle += 1;
        }
        //计算当前可显示的预约日期，并且最后一天显示即将放号倒计时
        List<Date> dateList = new ArrayList<>();
        for (int i = 0; i < cycle; i++) {
            //计算当前可显示的预约日期
            DateTime curDateTime = new DateTime().plusDays(i);
            String dateString = curDateTime.toString("yyyy-MM-dd");
            dateList.add(new DateTime(dateString).toDate());
        }
        return dateList;
    }

    @Override
    public Map<String, Object> getBookingScheduleRule(String hoscode, String depcode) {
        //获取医院
        Hospital hospital = hospitalRepository.getByHoscode(hoscode);
        //获取预约规则
        BookingRule bookingRule = hospital.getBookingRule();
        //根据预约规则获取可预约日期列表
        List<Date> dateList = this.getDateList(bookingRule);
        //查询条件：根据医院编号、科室编号以及预约日期查询
        Criteria criteria = Criteria.where("hoscode").is(hoscode).and("depcode").is(depcode).and("workDate").in(dateList);
        //根据工作日workDate期进行分组
        Aggregation agg = Aggregation.newAggregation(
                //查询条件
                Aggregation.match(criteria),
                Aggregation
                        //按照日期分组 select workDate as workDate from schedule group by workDate
                        .group("workDate").first("workDate").as("workDate")
                        //剩余预约数
                        .sum("availableNumber").as("availableNumber")
        );
        //执行查询实现类型转化List<BookingScheduleRuleVo>=》Map（k：workDate，v：BookingScheduleRuleVo
        AggregationResults<BookingScheduleRuleVo> aggResults = mongoTemplate.aggregate(agg, Schedule.class, BookingScheduleRuleVo.class);
        //获取查询结果
        List<BookingScheduleRuleVo> list = aggResults.getMappedResults();
        //将list转换成Map，日期为key，BookingScheduleRuleVo对象为value
        Map<Date, BookingScheduleRuleVo> scheduleVoMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(list)) {
            scheduleVoMap = list.stream().collect(
                    Collectors.toMap(bookingScheduleRuleVo -> bookingScheduleRuleVo.getWorkDate(), bookingScheduleRuleVo -> bookingScheduleRuleVo)
            );
        }
        //获取可预约排班规则
        List<BookingScheduleRuleVo> bookingScheduleRuleVoList = new ArrayList<>();
        int size = dateList.size();
        for (int i = 0; i < size; i++) {
            Date date = dateList.get(i);
            BookingScheduleRuleVo bookingScheduleRuleVo = scheduleVoMap.get(date);
            if (bookingScheduleRuleVo == null) { // 说明当天没有排班数据
                bookingScheduleRuleVo = new BookingScheduleRuleVo();
                bookingScheduleRuleVo.setWorkDate(date);
                //科室剩余预约数  -1表示无号
                bookingScheduleRuleVo.setAvailableNumber(-1);
            }
            bookingScheduleRuleVo.setWorkDateMd(date);
            //计算当前预约日期为周几
            String dayOfWeek = DateUtil.getDayOfWeek(new DateTime(date));
            bookingScheduleRuleVo.setDayOfWeek(dayOfWeek);
            if (i == size - 1) { //最后一条记录为即将放号
                bookingScheduleRuleVo.setStatus(1);
            } else {
                bookingScheduleRuleVo.setStatus(0);
            }

            //设置预约状态： 0正常； 1即将放号； -1当天已停止挂号
            if (i == 0) { //当天如果过了停挂时间， 则不能挂号
                DateTime stopTime = this.getDateTime(new DateTime(), bookingRule.getStopTime());
                if (stopTime.isBeforeNow()) {
                    bookingScheduleRuleVo.setStatus(-1);//停止挂号
                }
            }
            bookingScheduleRuleVoList.add(bookingScheduleRuleVo);
        }
        //医院基本信息
        Map<String, String> info = new HashMap<>();
        //医院名称
        info.put("hosname", hospitalRepository.findByHoscode(hoscode).getHosname());
        //科室
        Department department = departmentRepository.findByHoscodeAndDepcode(hoscode, depcode);
        //大科室名称
        info.put("bigname", department.getBigname());
        //科室名称
        info.put("depname", department.getDepname());
        //当前月份
        info.put("workDateString", new DateTime().toString("yyyy年MM月"));
        //放号时间
        info.put("releaseTime", bookingRule.getReleaseTime());
        Map<String, Object> result = new HashMap<>();
        //可预约日期数据
        result.put("bookingScheduleList", bookingScheduleRuleVoList);//排班日期列表
        result.put("info", info);//医院基本信息
        return result;
    }

    @Override
    public List<Schedule> getScheduleList(String hoscode, String depcode, String workDate) {

        //注意：最后一个参数需要进行数据类型的转换
        List<Schedule> scheduleList = scheduleRepository.findByHoscodeAndDepcodeAndWorkDate(
                hoscode,
                depcode,
                new DateTime(workDate).toDate());//数据类型的转换

        //id为ObjectId类型时需要进行转换

        for (Schedule schedule : scheduleList) {
            schedule.getParam().put("id", schedule.getId().toString());
        }
//        scheduleList.forEach(schedule -> {
//            schedule.getParam().put("id", schedule.getId().toString());
//        });

        return scheduleList;
    }
    @Override
    public Schedule getDetailById(String id) {
        Schedule schedule = scheduleRepository.findById(new ObjectId(id)).get();
        return this.packSchedule(schedule);
    }
    @Override
    public ScheduleOrderVo getScheduleOrderVo(String scheduleId) {

        Schedule schedule = this.getDetailById(scheduleId);
        String hosname = (String)schedule.getParam().get("hosname");
        String depname = (String)schedule.getParam().get("depname");

        ScheduleOrderVo scheduleOrderVo = new ScheduleOrderVo();
        scheduleOrderVo.setHoscode(schedule.getHoscode()); //医院编号
        scheduleOrderVo.setHosname(hosname); //医院名称
        scheduleOrderVo.setDepcode(schedule.getDepcode()); //科室编号
        scheduleOrderVo.setDepname(depname); //科室名称
        scheduleOrderVo.setHosScheduleId(schedule.getHosScheduleId()); //医院端的排班主键
        scheduleOrderVo.setAvailableNumber(schedule.getAvailableNumber()); //剩余预约数
        scheduleOrderVo.setTitle(hosname + depname + "挂号费");
        scheduleOrderVo.setReserveDate(schedule.getWorkDate()); //安排日期
        scheduleOrderVo.setReserveTime(schedule.getWorkTime()); //安排时间（0：上午 1：下午）
        scheduleOrderVo.setAmount(schedule.getAmount());//挂号费用

        //获取预约规则相关数据
        Hospital hospital = hospitalRepository.getByHoscode(schedule.getHoscode());
        BookingRule bookingRule = hospital.getBookingRule();
        String quitTime = bookingRule.getQuitTime();//退号时间
        //退号实际时间（如：就诊前一天为-1，当天为0）
        DateTime quitDay = new DateTime(schedule.getWorkDate()).plusDays(bookingRule.getQuitDay());//退号日期
        DateTime quitDateTime = this.getDateTime(quitDay, quitTime);//可退号的具体的日期和时间
        scheduleOrderVo.setQuitTime(quitDateTime.toDate());

        return scheduleOrderVo;
    }
    /**
     * 封装医院名称，科室名称和周几
     * @param schedule
     * @return
     */
    private Schedule packSchedule(Schedule schedule) {
        //医院名称
        String hosname = hospitalRepository.getByHoscode(schedule.getHoscode()).getHosname();
        //科室名称
        String depname = departmentRepository.findByHoscodeAndDepcode(schedule.getHoscode(),schedule.getDepcode()).getDepname();
        //周几
        String dayOfWeek = DateUtil.getDayOfWeek(new DateTime(schedule.getWorkDate()));

        Integer workTime = schedule.getWorkTime();
        String workTimeString = workTime.intValue() == 0 ? "上午" : "下午";

        schedule.getParam().put("hosname",hosname);
        schedule.getParam().put("depname",depname);
        schedule.getParam().put("dayOfWeek",dayOfWeek);
        schedule.getParam().put("workTimeString", workTimeString);

        //id为ObjectId类型时需要进行转换
        schedule.getParam().put("id",schedule.getId().toString());
        return schedule;
    }
    @Override
    public void updateByOrderMqVo(OrderMqVo orderMqVo) {
        //下单成功更新预约数
        ObjectId objectId = new ObjectId(orderMqVo.getScheduleId());
        //id是objectId
        Schedule schedule = scheduleRepository.findById(objectId).get();
        //id是string
        //      Schedule schedule = scheduleRepository.findById(orderMqVo.getScheduleId()).get();
        schedule.setReservedNumber(orderMqVo.getReservedNumber());
        schedule.setAvailableNumber(orderMqVo.getAvailableNumber());

        //主键一致就是更新
        scheduleRepository.save(schedule);
    }
}
