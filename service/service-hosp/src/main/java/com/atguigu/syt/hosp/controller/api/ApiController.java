package com.atguigu.syt.hosp.controller.api;

import com.atguigu.common.service.utils.HttpRequestHelper;
import com.atguigu.common.util.result.Result;
import com.atguigu.syt.hosp.service.DepartmentService;
import com.atguigu.syt.hosp.service.HospitalService;
import com.atguigu.syt.hosp.service.HospitalSetService;
import com.atguigu.syt.hosp.service.ScheduleService;
import com.atguigu.syt.model.hosp.Department;
import com.atguigu.syt.model.hosp.Hospital;
import com.atguigu.syt.model.hosp.Schedule;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Api(tags = "医院管理API接口")
@RestController
@RequestMapping("/api/hosp")
public class ApiController {
    @Autowired
    private HospitalService hospitalService;
    @Autowired
    private HospitalSetService hospitalSetService;
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private ScheduleService scheduleService;

//    @ApiOperation(value = "展示医院基本信息")
//    @PostMapping("/hospital/show")
//    public Result showHospital(HttpServletRequest request) {
//        Map<String, Object> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());
//        String hoscode = (String) paramMap.get("hoscode");
//        String signKey = hospitalSetService.getSignKey(hoscode);
//        HttpRequestHelper.checkSign(paramMap, signKey);
//        HospitalSet hospital = hospitalSetService.getByHoscode(hoscode);
//        return Result.ok(hospital);
//    }

    @ApiOperation(value = "上传医院基本信息")
    @PostMapping("/saveHospital")
    public Result saveHospital(HttpServletRequest request) {
        //request.getParameterMap()返回的是一个Map类型的值,该返回值记录着前端所提交请求中的请求参数和请求参数值的映射关系。
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());

        //签名验证
        String hoscode = (String) paramMap.get("hoscode");
        String signKey = hospitalSetService.getSignKey(hoscode);
        HttpRequestHelper.checkSign(paramMap, signKey);

        hospitalService.save(paramMap);
        return Result.ok();
    }

    @ApiOperation(value = "获取医院信息")
    @PostMapping("/hospital/show")
    public Result hospital(HttpServletRequest request) {
        //从request取出参数
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());

        //获取自己的签名key
        String hoscode = (String) paramMap.get("hoscode");
        String signKey = hospitalSetService.getSignKey(hoscode);
        //使用工具方法验签
        HttpRequestHelper.checkSign(paramMap, signKey);
//根据hoscode查询医院信息
        Hospital hospital = hospitalService.getByHoscode(hoscode);
        return Result.ok(hospital);
    }

    @ApiOperation(value = "上传科室")
    @PostMapping("/saveDepartment")
    public Result saveDepartment(HttpServletRequest request) {
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());

        //签名验证
        String hoscode = (String) paramMap.get("hoscode");
        String signKey = hospitalSetService.getSignKey(hoscode);
        HttpRequestHelper.checkSign(paramMap, signKey);

        departmentService.save(paramMap);
        return Result.ok();
    }

    @ApiOperation(value = "获取分页列表")
    @PostMapping("/department/list")
    public Result department(HttpServletRequest request) {
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());

        //签名验证
        String hoscode = (String) paramMap.get("hoscode");
        String signKey = hospitalSetService.getSignKey(hoscode);
        HttpRequestHelper.checkSign(paramMap, signKey);

        int page = Integer.parseInt((String) paramMap.get("page"));
        int limit = Integer.parseInt((String) paramMap.get("limit"));
        //page导包import org.springframework.data.domain.Page;
        Page<Department> pageModel = departmentService.selectPage(page, limit, hoscode);
        return Result.ok(pageModel);
    }

    @ApiOperation(value = "删除科室")
    @PostMapping("/department/remove")
    public Result removeDepartment(HttpServletRequest request) {
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());

        //签名验证
        String hoscode = (String) paramMap.get("hoscode");
        String signKey = hospitalSetService.getSignKey(hoscode);
        HttpRequestHelper.checkSign(paramMap, signKey);

        String depcode = (String) paramMap.get("depcode");
        departmentService.remove(hoscode, depcode);
        return Result.ok();
    }

    @ApiOperation(value = "上传排班")
    @PostMapping("/saveSchedule")
    public Result saveSchedule(HttpServletRequest request) {
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());

        //签名验证
        String hoscode = (String) paramMap.get("hoscode");
        String signKey = hospitalSetService.getSignKey(hoscode);
        HttpRequestHelper.checkSign(paramMap, signKey);

        scheduleService.save(paramMap);
        return Result.ok();
    }

    @ApiOperation(value = "获取排班分页列表")
    @PostMapping("/schedule/list")
    public Result schedule(HttpServletRequest request) {
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());

        //签名验证
        String hoscode = (String) paramMap.get("hoscode");
        String signKey = hospitalSetService.getSignKey(hoscode);
        HttpRequestHelper.checkSign(paramMap, signKey);

        int page = Integer.parseInt((String) paramMap.get("page"));
        int limit = Integer.parseInt((String) paramMap.get("limit"));
        Page<Schedule> pageModel = scheduleService.selectPage(page, limit, hoscode);
        return Result.ok(pageModel);
    }

    @ApiOperation(value = "删除排班")
    @PostMapping("/schedule/remove")
    public Result removeSchedule(HttpServletRequest request) {
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());

        //签名验证
        String hoscode = (String) paramMap.get("hoscode");
        String signKey = hospitalSetService.getSignKey(hoscode);
        HttpRequestHelper.checkSign(paramMap, signKey);

        String hosScheduleId = (String) paramMap.get("hosScheduleId");
        scheduleService.remove(hoscode, hosScheduleId);
        return Result.ok();
    }
}
