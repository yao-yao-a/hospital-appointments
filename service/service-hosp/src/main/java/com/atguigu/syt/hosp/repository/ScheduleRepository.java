package com.atguigu.syt.hosp.repository;

import com.atguigu.syt.model.hosp.Schedule;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;
import java.util.List;

public interface ScheduleRepository extends MongoRepository<Schedule, ObjectId> {
    /**
     * 根据医院编码和医院端排版id查询排班记录
     * @param hoscode
     * @param hosScheduleId
     * @return
     */
    Schedule findByHoscodeAndHosScheduleId(String hoscode, String hosScheduleId);
    /**
     * 据医院编号 、科室编号和工作日期，查询排班详细信息
     * @param hoscode
     * @param depcode
     * @param date
     * @return
     */
    List<Schedule> findByHoscodeAndDepcodeAndWorkDate(String hoscode, String depcode, Date date);
}
