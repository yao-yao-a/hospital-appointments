package com.atguigu.syt.hosp.service;

import com.atguigu.syt.model.hosp.Schedule;
import com.atguigu.syt.vo.hosp.ScheduleOrderVo;
import com.atguigu.syt.vo.order.OrderMqVo;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface ScheduleService {
    /**
     * 保存排班信息
     * @param paramMap
     */
    void save(Map<String, Object> paramMap);
    /**
     * 分页查询
     * @param page 当前页码
     * @param limit 每页记录数
     * @param hoscode 查询条件
     * @return
     */
    Page<Schedule> selectPage(int page, int limit, String hoscode);
    /**
     * 根据医院编码和排班id删除排班
     * @param hoscode
     * @param hosScheduleId
     */
    void remove(String hoscode, String hosScheduleId);
    /**
     * 根据医院编号 和 科室编号 ，查询排班规则（按日期展示）
     * @param page
     * @param limit
     * @param hoscode
     * @param depcode
     * @return
     */
    Map<String, Object> getScheduleRule(Long page, Long limit, String hoscode, String depcode);
    /**
     * 据医院编号 、科室编号和工作日期，查询排班详细信息
     * @param hoscode
     * @param depcode
     * @param workDate
     * @return
     */
    List<Schedule> getScheduleList(String hoscode, String depcode, String workDate);
    /**
     * 根据日期对象和时间字符串获取一个日期时间对象
     * @param
     * @param
     * @return
     */
    Map<String, Object> getBookingScheduleRule(String hoscode, String depcode);
    /**
     * 排班记录详情
     * @param id
     * @return
     */
    Schedule getDetailById(String id);
    /**
     * 获取排班相关数据
     * @param scheduleId
     * @return
     */
    ScheduleOrderVo getScheduleOrderVo(String scheduleId);
    /**
     * 更新排班数量
     * @param orderMqVo
     */
    void updateByOrderMqVo(OrderMqVo orderMqVo);
}
