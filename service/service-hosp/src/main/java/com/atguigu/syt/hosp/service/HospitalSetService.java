package com.atguigu.syt.hosp.service;

import com.atguigu.syt.model.hosp.HospitalSet;
import com.atguigu.syt.vo.hosp.HospitalSetQueryVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 医院设置表 服务类
 * </p>
 *
 * @author atguigu
 * @since 2023-07-04
 */
public interface HospitalSetService extends IService<HospitalSet> {

    Page<HospitalSet> selectPage(Long page, Long limit, HospitalSetQueryVo hospitalSetQueryVo);

    /**
     * 根据医院编码获取医院签名密钥
     *
     * @param hoscode
     * @return
     */
    String getSignKey(String hoscode);

    /**
     * 根据hoscode获取医院设置
     *
     * @param hoscode
     * @return
     */
    HospitalSet getByHoscode(String hoscode);

}
