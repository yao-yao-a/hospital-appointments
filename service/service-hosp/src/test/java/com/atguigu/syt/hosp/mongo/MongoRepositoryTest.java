package com.atguigu.syt.hosp.mongo;


import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MongoRepositoryTest {
    @Autowired
    private UserRepository userRepository;

    //插入
    @Test
    public void test() {
        User user = new User();
        user.setName("小谷");
        user.setAge(19);
        user.setCreateDate(new Date());
        userRepository.save(user);
    }

    //查询所有
    @Test
    public void testFindAll() {
        List<User> userList = userRepository.findAll();
        System.out.println(userList);
    }

    //根据id查询
    @Test
    public void testFindById() {

        Optional<User> optional = userRepository.findById(
                new ObjectId("6412fa11fa5cdd0300a2d4e4")
        );
        boolean present = optional.isPresent();
        if (present) {
            User user = optional.get();
            System.out.println(user);
        }
    }

    //条件查询
    @Test
    public void testFindAllExample() {

        User user = new User();
        user.setAge(19);
        Example<User> example = Example.of(user);
        List<User> userList = userRepository.findAll(example);
        System.out.println(userList);
    }

    //排序查询
    @Test
    public void testFindAllSort() {
        Sort sort = Sort.by(Sort.Direction.DESC, "age");
        List<User> userList = userRepository.findAll(sort);
        System.out.println(userList);
    }

    //分页查询
    @Test
    public void testFindAllPage() {

        PageRequest pageRequest = PageRequest.of(0, 10);
        Page<User> page = userRepository.findAll(pageRequest);
        int totalPages = page.getTotalPages();
        List<User> userList = page.getContent();
        System.out.println(userList);
        System.out.println(totalPages);
    }

    //更新
    @Test
    public void testUpdateUser() {

        //注意：先查询，再更新
        Optional<User> optional = userRepository.findById(
                new ObjectId("6412fa11fa5cdd0300a2d4e4")
        );
        if (optional.isPresent()) {
            User user = optional.get();
            user.setAge(100);
            //user中包含id，就会执行更新
            userRepository.save(user);
            System.out.println(user);
        }
    }

    //删除
    @Test
    public void testDeleteUser() {
        userRepository.deleteById(
                new ObjectId("6412fa11fa5cdd0300a2d4e4")
        );
    }
}
