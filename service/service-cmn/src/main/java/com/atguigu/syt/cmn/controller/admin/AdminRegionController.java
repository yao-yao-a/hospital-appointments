package com.atguigu.syt.cmn.controller.admin;


import com.alibaba.excel.EasyExcel;
import com.atguigu.common.util.result.Result;
import com.atguigu.syt.cmn.service.RegionService;
import com.atguigu.syt.model.cmn.Region;
import com.atguigu.syt.vo.cmn.RegionExcelVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author atguigu
 * @since 2023-07-08
 */
@Api(tags = "地区")
@RestController
@RequestMapping("/admin/cmn/region")
public class AdminRegionController {

    @Autowired
    private RegionService regionService;

    /**
     * 导出：需要读取所有的地区列表，需要把数据写入到excel表格
     * 文件下载并且失败的时候返回json（默认失败了会返回一个有部分数据的Excel）
     *
     * @since 2.1.1
     */
    @ApiOperation(value="导出")
    @GetMapping(value = "/exportData")
    public void downloadFailedUsingJson(HttpServletResponse response) throws IOException {
        // 需要读取所有的地区列表
        try {
            List<RegionExcelVo> regionExcelVoList = regionService.findRegionExcelVoList();
            // 如果需要导出excel表格，需要设置响应头 content-type  ： application/json text/html
            // mimetype：多媒体类型，http协议，tomcat
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            // 设置编码格式
            response.setCharacterEncoding("utf-8");
            // 在发送请求前，先把中文字符使用URLEncoder.encode()方法转码，中文乱码这个问题轻松解决。
//使用URLEncoder.encode()方法解决Tomcat发送HTTP请求中文参数乱码的问题
// 导出：下载文件的时候，需要给文件取一个名字在，   转码操作，怕出现乱码
            String fileName = URLEncoder.encode("数据字典", "UTF-8").replaceAll("\\+", "%20");
            //Content-disposition是MIME协议的扩展，MIME协议指示MIME用户代理如何显示附加的文件
            // 设置excel表格，添加附件
            response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
            // 前面都是准备工作，开始导出数据，实际上往excel表格进行写数据
            EasyExcel.write(response.getOutputStream(), RegionExcelVo.class).sheet("数据字典").doWrite(regionExcelVoList);
        } catch (Exception e) {
            System.out.println(ExceptionUtils.getStackTrace(e));
            // 重置response
            response.reset();
            response.setContentType("text/html");
            response.setCharacterEncoding("utf-8");
            response.getWriter().println("导出失败");
        }


    }

    @ApiOperation(value = "根据上级code获取子节点数据列表")
    @ApiImplicitParam(name = "parentCode", value = "上级节点code", required = true)
    @GetMapping(value = "/findRegionListByParentCode/{parentCode}")
    public Result<List<Region>> findRegionListByParentCode(@PathVariable String parentCode){
        List<Region> regionList = regionService.findRegionListByParentCode(parentCode);
        return Result.ok(regionList);
    }


}

