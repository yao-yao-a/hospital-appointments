package com.atguigu.syt.cmn.service.impl;

import com.atguigu.syt.cmn.mapper.DictMapper;
import com.atguigu.syt.cmn.service.DictService;
import com.atguigu.syt.cmn.service.DictTypeService;
import com.atguigu.syt.model.cmn.Dict;
import com.atguigu.syt.model.cmn.DictType;
import com.atguigu.syt.vo.cmn.DictTypeVo;
import com.atguigu.syt.vo.cmn.DictVo;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author atguigu
 * @since 2023-07-08
 */
@Service
public class DictServiceImpl extends ServiceImpl<DictMapper, Dict> implements DictService {

    @Autowired
    private DictTypeService dictTypeService;
    /**
     * 实现步骤
     * ① 查询dict_type  ：查询所有的数据字典类型
     * ② 查询dict ：查询所有的数据字典
     * @return
     */
    @Override
    public List<DictTypeVo> findAllDictList() {
        // 查询所有的数据类型
        List<DictType> dictTypeList = dictTypeService.list();
        // 查询所有的数据字典
        List<Dict> dictList = this.list();
        // 根据字典的类型进行筛选
        // 晒选完成之后，肯定需要使用集合进行接收，筛选完成之后，肯定需要在页面进行展示
        List<DictTypeVo> dictTypeVoList = new ArrayList<>();
        // 拆分字典的数据类型
        for (DictType dictType : dictTypeList) {
            // 创建字典类型，一会丢到集合里面
            DictTypeVo dictTypeVo = new DictTypeVo();
            // id用来进行区分,一共只有2个，一个医院等级，一个是证件类型
            dictTypeVo.setId("parent-"+dictType.getId());
            dictTypeVo.setName(dictType.getName());
            // 判断主外键是否相等
            // dict.getDictTypeId().longValue() : 获取到数据类型的id
            List<Dict> subDictList =
                    dictList.stream().filter(
                            dict -> dict.getDictTypeId().longValue() == dictType.getId().longValue()
                    ).collect(Collectors.toList());
            List<DictVo> children = new ArrayList<>();
            for (Dict dict : subDictList) {
                DictVo dictVo = new DictVo();
                dictVo.setId("children-"+dict.getId());
                dictVo.setName(dict.getName());
                dictVo.setValue(dict.getValue());
                children.add(dictVo);
            }
            dictTypeVo.setChildren(children);
            dictTypeVoList.add(dictTypeVo);

        }


        return dictTypeVoList;
    }
    @Override
    public String getNameByDictTypeIdAndValue(Long dictTypeId, String value) {

        LambdaQueryWrapper<Dict> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Dict::getDictTypeId, dictTypeId);
        queryWrapper.eq(Dict::getValue, value);
        Dict dict = baseMapper.selectOne(queryWrapper);
        if (dict != null) {
            return dict.getName();
        }
        return "";
    }
    @Override
    public List<Dict> findDictListByDictTypeId(Long dictTypeId) {
        LambdaQueryWrapper<Dict> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Dict::getDictTypeId, dictTypeId);
        return this.list(queryWrapper);
    }
}
