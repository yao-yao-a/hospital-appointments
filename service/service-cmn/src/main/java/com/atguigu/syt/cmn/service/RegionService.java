package com.atguigu.syt.cmn.service;

import com.atguigu.syt.model.cmn.Region;
import com.atguigu.syt.vo.cmn.RegionExcelVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author atguigu
 * @since 2023-07-08
 */
public interface RegionService extends IService<Region> {

    List<Region> findRegionListByParentCode(String parentCode);

    List<RegionExcelVo> findRegionExcelVoList();
    /**
     * 根据地区编码获取地区名称
     * @param code
     * @return
     */
    String getNameByCode(String code);
}
