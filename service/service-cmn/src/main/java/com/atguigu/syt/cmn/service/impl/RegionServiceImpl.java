package com.atguigu.syt.cmn.service.impl;

import com.alibaba.excel.util.ListUtils;
import com.atguigu.syt.cmn.mapper.RegionMapper;
import com.atguigu.syt.cmn.service.RegionService;
import com.atguigu.syt.model.cmn.Region;
import com.atguigu.syt.vo.cmn.RegionExcelVo;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author atguigu
 * @since 2023-07-08
 *
 * 200 : 成功
 * 400 ：参数错误
 * 403 ：没有权限
 * 405 ：请求方式错误
 * 404 ： 资源没有找到
 * 500 ：服务器错误
 * 503 ：没有找到服务器，服务器没有开，服务器名字写错
 */
@Service
public class RegionServiceImpl extends ServiceImpl<RegionMapper, Region> implements RegionService {

    @Autowired
    private RedisTemplate redisTemplate;
    private List<Region> regionList;

    /**
     * 需求：缓存地区数据
     * ① 如果redis里面没有数据，只能查询数据库
     * ② 如果redis有数据，直接返回redis里面的数据，如果redis没有数据，在查询数据库
     */
    /**
     * @Cacheable：
     * 在方法执行前查看是否有缓存对应的数据，
     * 如果有直接返回数据，如果没有，则调用方法获取数据返回，并缓存起来。
     *
     * value：缓存的名字
     * key：缓存的key
     * unless：条件符合则不缓存，是对出参进行判断，出参用#result表示
    unless="#result.size() == 0" ：regionList如果没有查询到就不缓存
    最后一个是条件判断，如果redis里面有数据 ，不会走下面的代码，如果redis里面没有数据，才会走下面的代码
     *
     * @param parentCode
     * @return
     */
    @Override
    @Cacheable(value = "regionList", key = "#parentCode", unless="#result.size() == 0")
    public List<Region> findRegionListByParentCode(String parentCode) {
        // 获取redis里面所有的数据
//        regionList = (List<Region>) redisTemplate.opsForValue().get("regionList:" + parentCode);
//
//        if (regionList!=null){
//            // 如果不等于空，说明redis缓存里面有数据
//            return regionList;
//        }
        LambdaQueryWrapper<Region> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Region::getParentCode,parentCode);
        // 根据条件进行查询，根据省查询市，根据市查询区
        // 把局部变量变成成员变量 ctrl + alt + f
        regionList = baseMapper.selectList(queryWrapper);

        // 判断当前的地区是否可以往下点击
        for (Region region : regionList) {
            // 如果级别小于3 说明当前的节点有孩子
          boolean isHasChildren =  region.getLevel() < 3 ? true : false ;
          region.setHasChildren(isHasChildren);
        }
//        redisTemplate.opsForValue().set("regionList:" + parentCode, regionList, 5, TimeUnit.MINUTES);

        return regionList;
    }

    @Override
    public List<RegionExcelVo> findRegionExcelVoList() {
        // 查询所有的地区列表
        List<Region> regionList = this.list();
        // 创建一个动态扩容的集合
        List<RegionExcelVo> regionExcelVoList = ListUtils.newArrayListWithCapacity(regionList.size());
        for (Region region : regionList) {
            RegionExcelVo regionExcelVo = new RegionExcelVo();
//            regionExcelVo.setName(region.getName());
            // 复制属性 简化开发
            // 第一个参数：表示来源
            // 第二个参数：表示目标对象
            BeanUtils.copyProperties(region,regionExcelVo);
            regionExcelVoList.add(regionExcelVo);
        }
        return regionExcelVoList;
    }
    @Override
    public String getNameByCode(String code) {

        LambdaQueryWrapper<Region> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Region::getCode, code);
        Region region = baseMapper.selectOne(queryWrapper);
        if (region != null) {
            return region.getName();
        }
        return "";
    }
}
