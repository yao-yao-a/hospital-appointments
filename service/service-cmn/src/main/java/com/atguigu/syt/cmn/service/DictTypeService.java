package com.atguigu.syt.cmn.service;

import com.atguigu.syt.model.cmn.DictType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author atguigu
 * @since 2023-07-08
 */
public interface DictTypeService extends IService<DictType> {

}
