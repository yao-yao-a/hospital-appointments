package com.atguigu.syt.cmn.controller.admin;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author atguigu
 * @since 2023-07-08
 */
@RestController
@RequestMapping("/admin/cmn/dictType")
public class AdminDictTypeController {

}

