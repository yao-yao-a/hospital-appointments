package com.atguigu.syt.cmn.service;

import com.atguigu.syt.model.cmn.Dict;
import com.atguigu.syt.vo.cmn.DictTypeVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author atguigu
 * @since 2023-07-08
 */
public interface DictService extends IService<Dict> {

    List<DictTypeVo> findAllDictList();
    /**
     * 根据数据字典类别和字典值获取字典名称
     * @param dictTypeId
     * @param value
     * @return
     */
    String getNameByDictTypeIdAndValue(Long dictTypeId, String value);
    /**
     * 根据数据字典类别和字典值获取字典列表
     * @param dictTypeId
     * @return
     */
    List<Dict> findDictListByDictTypeId(Long dictTypeId);
}
