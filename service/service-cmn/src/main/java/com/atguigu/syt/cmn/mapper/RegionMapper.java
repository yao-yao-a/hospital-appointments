package com.atguigu.syt.cmn.mapper;

import com.atguigu.syt.model.cmn.Region;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author atguigu
 * @since 2023-07-08
 */
public interface RegionMapper extends BaseMapper<Region> {

}
