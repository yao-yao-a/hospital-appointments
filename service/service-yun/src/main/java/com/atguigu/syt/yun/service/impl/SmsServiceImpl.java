package com.atguigu.syt.yun.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.common.service.exception.GuiguException;
import com.atguigu.common.util.result.ResultCodeEnum;
import com.atguigu.syt.vo.sms.SmsVo;
import com.atguigu.syt.yun.service.SmsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;
import com.atguigu.syt.yun.util.HttpUtils;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
@Slf4j
public class SmsServiceImpl implements SmsService {
    @Override
    public void send(SmsVo smsVo) {
        String host = "https://miitangs09.market.alicloudapi.com";
        String path = "/v1/tools/sms/code/sender";
        String method = "POST";
        String appcode = "f21eb97ce792410e8e513d706ece1fdb";
        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        //根据API的要求，定义相对应的Content-Type
        headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        //需要给X-Ca-Nonce的值生成随机字符串，每次请求不能相同
        headers.put("X-Ca-Nonce", UUID.randomUUID().toString());
        Map<String, String> querys = new HashMap<String, String>();
        Map<String, String> bodys = new HashMap<String, String>();
        bodys.put("filterVirtual", "false");
        bodys.put("phoneNumber", smsVo.getPhone());
        bodys.put("reqNo", "miitangtest01");
        bodys.put("smsSignId", "0000");
        //bodys.put("smsTemplateNo", "smsTemplateNo");
        //bodys.put("smsTemplateNo", smsVo.getTemplateCode());
        bodys.put("verifyCode", (String) smsVo.getParam().get("code"));


        try {
            /**
             * 重要提示如下:
             * HttpUtils请从
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
             * 下载
             *
             * 相应的依赖请参照
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
             */
            HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
            System.out.println(response.toString());
            //获取response的body
            String result = EntityUtils.toString(response.getEntity());
            System.out.println(result);
            HashMap<String, String> resultMap = JSONObject.parseObject(result, HashMap.class);
            String code = resultMap.get("code");
            if(!"FP00000".equals(code)){
                String message = resultMap.get("message");
                log.error("短信发送失败：code = " + code + ", message = " + message);
                throw new GuiguException(ResultCodeEnum.FAIL.getCode(), "短信发送失败");
            }

        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            throw new GuiguException(ResultCodeEnum.FAIL.getCode(), "短信发送失败");
        }
    }
}
