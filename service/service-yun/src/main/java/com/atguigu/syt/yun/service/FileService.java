package com.atguigu.syt.yun.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface FileService {

    /**
     * 文件上传
     * @param file
     * @return
     */
    Map<String, String> upload(MultipartFile file);
    /**
     * 获取图片url地址
     * @param objectName
     * @return
     */
    String getPreviewUrl(String objectName);
}