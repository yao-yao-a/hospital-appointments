package com.atguigu.syt.yun.controller.front;

import com.atguigu.common.util.result.Result;
import com.atguigu.syt.vo.sms.SmsVo;
import com.atguigu.syt.yun.service.SmsService;
import com.atguigu.syt.yun.util.RandomUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


@Api(tags = "短信接口")
@RestController
@RequestMapping("/front/yun/sms")
public class FrontSmsController {
    @Autowired
    private SmsService smsService;

    @Autowired
    private RedisTemplate redisTemplate;
    @ApiOperation("发送短信")
    @ApiImplicitParam(name = "phone",value = "手机号")
    @PostMapping("/send/{phone}")
    public Result send(@PathVariable String phone) {
        //1获取验证码、设置时效
        int minutes = 5; //验证码5分钟有效
        String code = RandomUtil.getSixBitRandom();
        //2把参数存入对象，SmsVo
        SmsVo smsVo = new SmsVo();
        smsVo.setPhone(phone);
        smsVo.setTemplateCode("CST_qozfh101");
        Map<String,Object> paramsMap = new HashMap<String, Object>(){{
            put("code", code);
            put("expire_at", minutes);
        }};
        smsVo.setParam(paramsMap);
        //3调用服务发送验证码
        smsService.send(smsVo);
        //4向redis存入验证码，设置时效
        redisTemplate.opsForValue().set("code:"+phone,code,minutes, TimeUnit.MINUTES);
        return Result.ok();
    }
}
