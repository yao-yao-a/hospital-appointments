package com.atguigu.syt.yun.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;
import com.atguigu.common.service.exception.GuiguException;
import com.atguigu.common.util.result.ResultCodeEnum;
import com.atguigu.syt.yun.service.FileService;
import com.atguigu.syt.yun.util.OssConstantProperties;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
@Slf4j
public class FileServiceImpl implements FileService {
    @Resource
    private OssConstantProperties ossConstantProperties;
    @Override
    public Map<String, String> upload(MultipartFile file) {
        //1准备OSS相关参数
        // Endpoint以华东1（杭州）为例，其它Region请按实际情况填写。
        String endpoint = ossConstantProperties.getEndpoint();
        // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
        String accessKeyId = ossConstantProperties.getKeyId();
        String accessKeySecret = ossConstantProperties.getKeySecret();
        // 填写Bucket名称，例如examplebucket。
        String bucketName = ossConstantProperties.getBucketName();
        // 填写Object完整路径，完整路径中不能包含Bucket名称，例如exampledir/exampleobject.txt。
        String filename = file.getOriginalFilename();
        //考虑（1）文件名不能重复，在文件名前加UUID （2）在文件名前加，日期路径
        String uuid = UUID.randomUUID().toString().replaceAll("-","");
        String path = new DateTime().toString("yyyyMMdd");
        String objectName = path + "/" + uuid
                + filename.substring(filename.lastIndexOf("."));

        //2创建客户端对象
        OSS ossClient = new OSSClientBuilder().build(endpoint,accessKeyId,accessKeySecret);


        try {
            //3创建请求对象，存入相关参数
            InputStream inputStream = file.getInputStream();
            // 创建PutObjectRequest对象。
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, objectName, inputStream);
// 设置该属性可以返回response。如果不设置，则返回的response为空。
            putObjectRequest.setProcess("true");
            //4使用客户端对象方法，发送请求获取结果
            PutObjectResult result = ossClient.putObject(putObjectRequest);
            //5判断是否上传失败
            log.info(Integer.toString(result.getResponse().getStatusCode()));
            if(result.getResponse().getStatusCode() != 200){
                throw new GuiguException(ResultCodeEnum.FAIL);
            }
            //6使用客户端方法，获取临时访问url
            // 设置签名URL过期时间，单位为毫秒。本示例以设置过期时间为1小时为例。
            Date expiration = new Date(new Date().getTime() + 60 * 60 * 1000);
            // 生成以GET方法访问的签名URL，访客可以直接通过浏览器访问相关内容。
            URL url = ossClient.generatePresignedUrl(bucketName, objectName, expiration);
            //7封装数据
            Map<String, String> map = new HashMap<>();
            map.put("previewUrl", url.toString()); //页面中授权预览图片
            map.put("url", objectName); //数据库存储

            return map;
        } catch (Exception e) {
            throw new GuiguException(ResultCodeEnum.FAIL, e);
        }finally {
            //8关闭客户端
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
    }
    @Override
    public String getPreviewUrl(String objectName) {
        //1准备OSS相关参数
        // Endpoint以华东1（杭州）为例，其它Region请按实际情况填写。
        String endpoint = ossConstantProperties.getEndpoint();
        // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。
        String accessKeyId = ossConstantProperties.getKeyId();
        String accessKeySecret = ossConstantProperties.getKeySecret();
        // 填写Bucket名称，例如examplebucket。
        String bucketName = ossConstantProperties.getBucketName();

        //2创建客户端对象
        OSS ossClient = new OSSClientBuilder().build(endpoint,accessKeyId,accessKeySecret);
        try {
            //3使用客户端方法，获取临时访问url
            // 设置签名URL过期时间，单位为毫秒。本示例以设置过期时间为1小时为例。
            Date expiration = new Date(new Date().getTime() + 60 * 60 * 1000);
            // 生成以GET方法访问的签名URL，访客可以直接通过浏览器访问相关内容。
            URL url = ossClient.generatePresignedUrl(bucketName, objectName, expiration);

            return url.toString();
        }catch (GuiguException ce) {
            System.out.println("Error Message:" + ce.getMessage());
            throw new GuiguException(ResultCodeEnum.FAIL, ce);
        }
        finally {
            //8关闭客户端
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
    }
}