package com.atguigu.syt.yun.service;

import com.atguigu.syt.vo.sms.SmsVo;

public interface SmsService {
        /**
         * 发送短信
         *
         * @param smsVo
         */
        void send(SmsVo smsVo);
    }

