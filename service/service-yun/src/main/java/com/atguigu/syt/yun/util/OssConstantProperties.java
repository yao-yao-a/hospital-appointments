package com.atguigu.syt.yun.util;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix="aliyun.oss") //读取节点
@Data
public class OssConstantProperties {
    
    private String endpoint;
    private String keyId;
    private String keySecret;
    private String bucketName;
}