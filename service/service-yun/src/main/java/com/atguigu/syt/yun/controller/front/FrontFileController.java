package com.atguigu.syt.yun.controller.front;

import com.atguigu.common.service.utils.AuthContextHolder;
import com.atguigu.common.util.result.Result;
import com.atguigu.syt.yun.service.FileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Api(tags = "阿里云文件管理")
@RestController
@RequestMapping("/front/yun/file")
public class FrontFileController {
    @Autowired
    private AuthContextHolder authContextHolder;
    @Autowired
    private FileService fileService;
    /**
     * 文件上传
     */
    @ApiOperation("文件上传")
    @ApiImplicitParam(name = "file",value = "上传文件", required = true)
    @PostMapping("/auth/upload")
    public Result<Map<String, String>> upload(MultipartFile file, HttpServletRequest request, HttpServletResponse response) {
        Map<String, String> map = fileService.upload(file);
        authContextHolder.checkAuth(request,response);
        return Result.ok(map);
    }
}