package com.atguigu.common.service.utils;

import com.atguigu.common.service.exception.GuiguException;
import com.atguigu.common.util.result.ResultCodeEnum;
import com.atguigu.syt.vo.user.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * 授权校验
 */
@Component
public class AuthContextHolder {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 校验token是否存在并返回UserId
     * @param request
     */
    /**
     * 检查授权状态并续期
     * @param request
     * @return
     */
    public Long checkAuth(HttpServletRequest request, HttpServletResponse response){
        //从http请求头中获取token

        String token = CookieUtils.getCookie(request, "token");
        if(StringUtils.isEmpty(token)) {
            //throw new GuiguException(ResultCodeEnum.LOGIN_AUTH);
            return refreshToken(request, response);//刷新token
        }

        Object userVoObj = redisTemplate.opsForValue().get("user:token:" + token);
        if(userVoObj == null){
            //throw new GuiguException(ResultCodeEnum.LOGIN_AUTH);
            return refreshToken(request, response);//刷新token
        }

        UserVo userVo = (UserVo)userVoObj;

        return userVo.getUserId();
    }
    /**
     * 刷新token
     * @param request
     * @param response
     * @return
     */
    public Long refreshToken(HttpServletRequest request, HttpServletResponse response) {

        //从cookie中获取刷新token
        String refreshToken = CookieUtils.getCookie(request, "refreshToken");

        //从redis中根据刷新token获取用户信息
        Object userVoObj = redisTemplate.opsForValue().get("user:refreshToken:" + refreshToken);
        if(userVoObj == null) {
            //LOGIN_AURH(214, "登录过期"),
            throw new GuiguException(ResultCodeEnum.LOGIN_TIMEOUT);//登录过期
        }

        UserVo userVo = (UserVo) userVoObj;
        saveToken(userVo, response);

        return userVo.getUserId();
    }
    /**
     * 将token和refreshToken保存在redis和cookie中的通用方法
     * @param userVo
     * @param response
     */
    public void saveToken(UserVo userVo, HttpServletResponse response) {

        int redisMaxTime = 30;
        //生成token/refreshToken
        String token = getToken();
        String refreshToken = getToken();

        //将生成token/refreshToken存入redis：token做键，userVo做值
        redisTemplate.opsForValue()//30分钟
                .set("user:token:" + token, userVo, redisMaxTime, TimeUnit.MINUTES);
        redisTemplate.opsForValue()//60分钟
                .set("user:refreshToken:" + refreshToken, userVo, redisMaxTime * 2, TimeUnit.MINUTES);

        //将token、refreshToken和name存入cookie
        int cookieMaxTime = 60 * 30;//30分钟
        CookieUtils.setCookie(response, "token", token, cookieMaxTime);
        CookieUtils.setCookie(response, "refreshToken", refreshToken, cookieMaxTime * 2);
        CookieUtils.setCookie(response, "name", URLEncoder.encode(userVo.getName()), cookieMaxTime * 2);
        CookieUtils.setCookie(response, "headimgurl", URLEncoder.encode(userVo.getHeadimgurl()), cookieMaxTime * 2);
    }


    private String getToken(){
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}