package com.atguigu.syt.cmn.client.impl;

import com.atguigu.syt.cmn.client.DictFeignClient;
import org.springframework.stereotype.Component;

@Component
public class DictDegradeFeignClient implements DictFeignClient {
    @Override
    public String getName(Long dictTypeId, String value) {
        return "数据获取失败";
    }
}
