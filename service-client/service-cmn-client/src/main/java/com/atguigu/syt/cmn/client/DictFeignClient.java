package com.atguigu.syt.cmn.client;

import com.atguigu.syt.cmn.client.impl.DictDegradeFeignClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
@FeignClient(
        value = "service-cmn",
        contextId = "dictFeignClient",
        fallback = DictDegradeFeignClient.class
)
public interface DictFeignClient {

    /**
     * 获取数据字典名称
     * @param dictTypeId
     * @param value
     * @return
     */
    @GetMapping(value = "/inner/cmn/dict/getName/{dictTypeId}/{value}")
    String getName(
            @PathVariable("dictTypeId") Long dictTypeId,
            @PathVariable("value") String value
    );

}