package com.atguigu.syt.cmn.client.impl;

import com.atguigu.syt.cmn.client.RegionFeignClient;
import org.springframework.stereotype.Component;

@Component
public class RegionDegradeFeignClient implements RegionFeignClient {
    @Override
    public String getName(String code) {
        return "数据获取失败";
    }
}
