package com.atguigu.syt.cmn.client;

import com.atguigu.syt.cmn.client.impl.RegionDegradeFeignClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(
        value = "service-cmn",
        contextId = "regionFeignClient",
        fallback = RegionDegradeFeignClient.class
)
public interface RegionFeignClient {

    /**
     * 根据地区编码获取地区名称
     * @param code
     * @return
     */
    @GetMapping(value = "/inner/cmn/region/getName/{code}")
    String getName(@PathVariable("code") String code);

}