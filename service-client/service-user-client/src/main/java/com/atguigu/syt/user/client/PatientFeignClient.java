package com.atguigu.syt.user.client;

import com.atguigu.syt.model.user.Patient;
import com.atguigu.syt.user.client.impl.PatientDegradeFeignClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(
        value = "service-user",
        contextId = "patientFeignClient",
        fallback = PatientDegradeFeignClient.class)
public interface PatientFeignClient {
    /**
     * 获取就诊人
     * @param id
     * @return
     */
    @GetMapping("/inner/user/patient/get/{id}")
    Patient getPatient(@PathVariable Long id);
}
