package com.atguigu.syt.yun.client;

import com.atguigu.syt.yun.client.impl.FileDegradeFeignClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(
        value = "service-yun",
        contextId = "fileFeignClient",
        fallback = FileDegradeFeignClient.class
)
public interface FileFeignClient {

    @GetMapping("inner/yun/file/getPreviewUrl")
    String getPreviewUrl(@RequestParam String objectName);
}