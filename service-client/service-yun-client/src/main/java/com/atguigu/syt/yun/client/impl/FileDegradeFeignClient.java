package com.atguigu.syt.yun.client.impl;

import com.atguigu.syt.yun.client.FileFeignClient;
import org.springframework.stereotype.Component;

@Component
public class FileDegradeFeignClient implements FileFeignClient {
    @Override
    public String getPreviewUrl(String objectName) {
        return "图片显示失败";
    }
}